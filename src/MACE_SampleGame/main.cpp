/**
 * @file main.cpp
 * @brief The main file for the MACE test game.
 * @author Joshua McLean
 * @date 2012/10/09 0:12
 *
 * Contains the entry point function and some utility functions for the construction of a game that
 * tests the capabilities of MACE.
 */

#include <iostream>

// the basic include file for mace; includes all files needed to access the engine's features
#include "mace/mace.h"


void error( 
    const std::string &a_prefix, 
    const char *a_what 
);
void run();


/**
 * @fn main( uint32_t, char ** )
 * @brief The main entry point for the test game.
 * @param argc The number of arguments passed to the program.
 * @param argv An array of C-style strings containing the arguments passed to the program.
 * @return Zero for successful run, non-zero for error.
 * @author Joshua McLean
 * @date 2012/10/08 23:35
 *
 * Try to run the game. Catch any thrown exceptions, pausing to allow the user to read the error
 * message if one is thrown. Then, return to the OS with the given return value.
 */
int
main( uint32_t argc, char **argv ) {
    std::cout << "MACE Test Game" << "\n"
        << "(c)2012 Joshua McLean" << "\n";

    uint32_t ret = 0;

    try {
        run();
        ret = 0;
    } catch( const Ogre::Exception &ex ) {
        error( "OGRE ERROR:", ex.what() );
        
        ret = 3;
    } catch( const mace::Exception &ex ) {
        error( "MACE ERROR:", ex.what() );
        ret = 2;
    } catch( const std::exception &ex ) {
        error( "STD ERROR:", ex.what() );
        ret = 1;
    }

    // wait for keypress if we received an error
    if( ret != 0 ) {
        system( "PAUSE" );
    }

    return ret;
}


/**
 * @fn error( const std::string &, const char * )
 * @brief Report an error.
 * @param a_prefix The prefixed message for the error.
 * @param a_what A C-style string containing the error that occurred.
 * @author Joshua McLean
 * @date 2012/10/08 23:37
 *
 * Print the prefix, then the error message.
 */
void 
error( const std::string &a_prefix, const char *a_what ) {
    boost::format formatter( "%s %s" );
    formatter % a_prefix.c_str();
    formatter % a_what;
    mace::Log::message( formatter.str() );
}


/**
 * @fn makeEntity( mace::Entity * const, const mace::Vector3 &, const mace::Vector3 & )
 * @brief Create an entity cloned from the given template of the given size at the given position.
 * @param a_pTemplateEnt A pointer to the entity to be used as a template.
 * @param a_rSizeVec The size for the clone.
 * @param a_rCenter The center position for the clone.
 * @author Joshua McLean
 * @date 2012/10/08 23:37
 *
 * Clone the target entity. Then, find its model and physics components. Adjust parameters for these
 * components to match the template entity.
 */
mace::Entity * const
makeEntity( 
    mace::Entity * const a_pTemplateEnt, 
    const mace::Vector3 &a_rSizeVec, 
    const mace::Vector3 &a_rCenter 
) {
    using namespace mace;

    auto pEntity = a_pTemplateEnt->clone( "fire square" );

    auto pModelComp = pEntity->findChildOfType< Primitive3dComponent >();
    auto pPhysicsComp = pEntity->findChildOfType< Physics3dComponent >();

    if( pModelComp == nullptr || pPhysicsComp == nullptr ) {
        throw std::exception( "Could not look up successfully." );
    }

    pModelComp->setMaterialName( "test/fire" );
    pModelComp->setSize( a_rSizeVec );
    pModelComp->setCenter( a_rCenter );
    pPhysicsComp->setTargetModel( pModelComp );

    return pEntity;
}


/**
 * @fn run()
 * @brief Run the game.
 * @author Joshua McLean
 * @date 2012/10/08 23:39
 *
 * Create the test game, adding, loading, and creating default systems. Set the OGRE log to minimal
 * output. Set up the physics simulation system. Using a slew of constants, create a number of cube
 * entities above the view of the camera. Then, create a pillar entity beneath them. Construct a 
 * floor underneath the pillar, then construct walls around it. Add these entities to a stage, then
 * set that stage as the current stage for the constructed game. Finally, execute the game.
 */
void run() {
    using namespace mace;

    // make the game
    auto pGame = new Game( "MACE Test Game" );
    pGame->addDefaultSystems();
    pGame->load();
    pGame->create();

    // set up log
    Ogre::LogManager::getSingletonPtr()->getDefaultLog()->setLogDetail( Ogre::LL_LOW );
    Ogre::LogManager::getSingletonPtr()->getDefaultLog()->setDebugOutputEnabled( false );

    // set up physics
    auto pSimulation = Component::findComponentOfType< Simulation >();
    pSimulation->setGravity( mace::Vector3( 0.0f, -9.80665f, 0.0f ) );

    // note: this value is cubed to determine the number of cubes
    const uint32_t NUM_OBJ = 2;

    const float SIZE = 2.0f;
    const float WALL_SIZE_MULT = 16.0f;
    const float SPACE = SIZE * 2.0f;
    const float START_X = 0.0f;
    const float START_Y = ( WALL_SIZE_MULT - 1 ) * SIZE;
    const float START_Z = -80.0f;
    const float FIRE_MASS = 1.0f;
    const float TOWER_MASS = 0.5f;
    const float WALL_MASS = 100.0f;

    auto pGraphics = Component::findComponentOfType< Graphics >();
    auto pSceneManager = pGraphics->getSceneManager();
    auto pCamera = pSceneManager->getCamera( "CAMERA" );
    pCamera->pitch( Ogre::Degree( -35 ) );

    // make the stage
    auto pPhysicsStage = new Stage( "physics stage" );

    {   // fire square
        auto pFireSquareTemplate = new Entity( "fire square template" );

        auto pModelComp = new Primitive3dComponent( "", Graphics::SHAPE_CUBE );
        pModelComp->setColor( mace::Color( 1.0f, 0.0f, 0.0f ) );
        pModelComp->setMaterialName( "test/fire" );

        auto pPhysicsComp = new Physics3dComponent();
        pPhysicsComp->setMass( FIRE_MASS );

        pFireSquareTemplate->addComponent( pModelComp );
        pFireSquareTemplate->addComponent( pPhysicsComp );

        for( int x = 0; x < NUM_OBJ; ++x ) {
            for( int y = 0; y < NUM_OBJ; ++y ) {
                for( int z = 0; z < NUM_OBJ; ++z ) {   
                    auto pEntity = makeEntity(
                        pFireSquareTemplate,
                        mace::Vector3( SIZE ),
                        mace::Vector3( 
                            -SPACE * NUM_OBJ / 2.0f + SPACE * x + START_X, 
                            -SPACE * NUM_OBJ / 2.0f + SPACE * y + START_Y,
                            -SPACE * NUM_OBJ / 2.0f + SPACE * z + START_Z
                        )
                    );

                    pPhysicsStage->addEntity( pEntity );
                }
            }
        }

        // done with template
        auto children = pFireSquareTemplate->getChildComponents();
        for( auto iter = children.begin(); iter != children.end(); ++iter ) {
            delete *iter;
        }
        delete pFireSquareTemplate;
    }

    // create tower
    auto pTowerSquare = new Entity( "tower" );
    {
        auto pModelComp = new Primitive3dComponent( "", Graphics::SHAPE_CUBE );
        pModelComp->setCenter( mace::Vector3( -SIZE, -50.0f + SIZE * 4.0f, START_Z - SIZE ) );
        pModelComp->setColor( mace::Color( 0.0f, 1.0f, 0.0f ) );
        pModelComp->setSize( mace::Vector3( 
            WALL_SIZE_MULT / 8.0f, 
            WALL_SIZE_MULT / 2.0f, 
            WALL_SIZE_MULT / 8.0f
        ) );
        pModelComp->setMaterialName( "test/wall" );
        pTowerSquare->addComponent( pModelComp );

        auto pPhysicsComp = new Physics3dComponent();
        pPhysicsComp->setMass( TOWER_MASS );
        pPhysicsComp->setTargetModel( pModelComp );
        pTowerSquare->addComponent( pPhysicsComp );

        auto pMoveXComp = new MoveXComponent();
        pMoveXComp->setTargetPhysics( pPhysicsComp );
        pTowerSquare->addComponent( pMoveXComp );

        auto pMoveZComp = new MoveZComponent();
        pMoveZComp->setTargetPhysics( pPhysicsComp );
        pTowerSquare->addComponent( pMoveZComp );
    }
    pPhysicsStage->addEntity( pTowerSquare );

    // create floor
    auto pFloorSquare = new Entity( "floor" );
    {
        auto pModelComp = new Primitive3dComponent( "", Graphics::SHAPE_CUBE );
        pModelComp->setCenter( mace::Vector3( -SIZE, -50.0f - SIZE, START_Z - SIZE ) );
        pModelComp->setColor( mace::Color( 0.0f, 0.0f, 1.0f ) );
        pModelComp->setSize( mace::Vector3( SIZE * WALL_SIZE_MULT, SIZE, SIZE * WALL_SIZE_MULT ) );
        pModelComp->setMaterialName( "test/floor" );

        auto pPhysicsComp = new Physics3dComponent();
        pPhysicsComp->setMass( 0.0f );
        pPhysicsComp->setTargetModel( pModelComp );

        pFloorSquare->addComponent( pModelComp );
        pFloorSquare->addComponent( pPhysicsComp );
    }
    pPhysicsStage->addEntity( pFloorSquare );

    // create left wall
    auto pLeftWall = new Entity( "left wall" );
    {
        auto floorModel = pFloorSquare->findChildOfType< Primitive3dComponent >();
        auto floorCenter = floorModel->getCenter();
        auto floorSize = floorModel->getSize();

        auto pModelComp = new Primitive3dComponent( "", Graphics::SHAPE_CUBE );
        mace::Vector3 wallSize( SIZE, SIZE * ( WALL_SIZE_MULT - 2 ), SIZE * WALL_SIZE_MULT );
        pModelComp->setCenter( mace::Vector3( 
            floorCenter.x + wallSize.x - floorSize.x,
            floorCenter.y + wallSize.y + floorSize.y,
            floorCenter.z
        ) );
        pModelComp->setColor( mace::Color( 0.0f, 0.0f, 1.0f ) );
        pModelComp->setSize( wallSize );
        pModelComp->setMaterialName( "test/wall" );

        auto pPhysicsComp = new Physics3dComponent();
        pPhysicsComp->setMass( WALL_MASS );
        pPhysicsComp->setTargetModel( pModelComp );

        pLeftWall->addComponent( pModelComp );
        pLeftWall->addComponent( pPhysicsComp );
    }
    pPhysicsStage->addEntity( pLeftWall );

    // create right wall
    auto pRightWall = new Entity( "right wall" );
    {
        auto floorModel = pFloorSquare->findChildOfType< Primitive3dComponent >();
        auto floorCenter = floorModel->getCenter();
        auto floorSize = floorModel->getSize();

        auto pModelComp = new Primitive3dComponent( "", Graphics::SHAPE_CUBE );
        mace::Vector3 wallSize( SIZE, SIZE * ( WALL_SIZE_MULT - 1 ), SIZE * WALL_SIZE_MULT );
        pModelComp->setCenter( mace::Vector3( 
            floorCenter.x - wallSize.x + floorSize.x,
            floorCenter.y + wallSize.y + floorSize.y,
            floorCenter.z
        ) );
        pModelComp->setColor( mace::Color( 0.0f, 0.0f, 1.0f ) );
        pModelComp->setSize( wallSize );
        pModelComp->setMaterialName( "test/wall" );

        auto pPhysicsComp = new Physics3dComponent();
        pPhysicsComp->setMass( WALL_MASS );
        pPhysicsComp->setTargetModel( pModelComp );

        pRightWall->addComponent( pModelComp );
        pRightWall->addComponent( pPhysicsComp );
    }
    pPhysicsStage->addEntity( pRightWall );

    // create back wall
    auto pBackWall = new Entity( "back wall" );
    {
        auto floorModel = pFloorSquare->findChildOfType< Primitive3dComponent >();
        auto floorCenter = floorModel->getCenter();
        auto floorSize = floorModel->getSize();

        auto pModelComp = new Primitive3dComponent( "", Graphics::SHAPE_CUBE );
        mace::Vector3 wallSize( SIZE * ( WALL_SIZE_MULT - 2 ), SIZE * ( WALL_SIZE_MULT - 1 ), SIZE );
        pModelComp->setCenter( mace::Vector3( 
            floorCenter.x,
            floorCenter.y + wallSize.y + floorSize.y,
            floorCenter.z + wallSize.z - floorSize.z
        ) );
        pModelComp->setColor( mace::Color( 0.0f, 0.0f, 1.0f ) );
        pModelComp->setSize( wallSize );
        pModelComp->setMaterialName( "test/wall" );

        auto pPhysicsComp = new Physics3dComponent();
        pPhysicsComp->setMass( WALL_MASS );
        pPhysicsComp->setTargetModel( pModelComp );

        pBackWall->addComponent( pModelComp );
        pBackWall->addComponent( pPhysicsComp );
    }
    pPhysicsStage->addEntity( pBackWall );

    // create front wall
    auto pFrontWall = new Entity( "front wall" );
    {
        auto floorModel = pFloorSquare->findChildOfType< Primitive3dComponent >();
        auto floorCenter = floorModel->getCenter();
        auto floorSize = floorModel->getSize();

        auto pModelComp = new Primitive3dComponent( "", Graphics::SHAPE_CUBE );
        mace::Vector3 wallSize( SIZE * ( WALL_SIZE_MULT - 2 ), SIZE * ( WALL_SIZE_MULT - 1 ), SIZE );
        pModelComp->setCenter( mace::Vector3( 
            floorCenter.x,
            floorCenter.y + wallSize.y + floorSize.y,
            floorCenter.z - wallSize.z + floorSize.z
        ) );
        pModelComp->setColor( mace::Color( 0.0f, 0.0f, 1.0f ) );
        pModelComp->setSize( wallSize );
        pModelComp->setMaterialName( "test/wireframe" );

        auto pPhysicsComp = new Physics3dComponent();
        pPhysicsComp->setMass( WALL_MASS );
        pPhysicsComp->setTargetModel( pModelComp );

        pFrontWall->addComponent( pModelComp );
        pFrontWall->addComponent( pPhysicsComp );
    }
    pPhysicsStage->addEntity( pFrontWall );

    // set the stage - also loads entities + components
    pGame->replaceStage( pPhysicsStage );

    // load the stage
    pPhysicsStage->load();

    // create the stage
    pPhysicsStage->create();

    // run the game (disposes of game when done)
    execute( pGame );
}
