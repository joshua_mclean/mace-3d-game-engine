#include "mace/Vertex.h"


//
// PUBLIC CTOR
//

/**
 * @fn Vertex::Vertex( const Vector3 &, const Color &, const Vector3 & )
 * @brief Constructor.
 * @param a_rPosition The position of the vertext.
 * @param a_rColor The color of the vertex (default black).
 * @param a_rNormal The normal vector for the vertex (default unit y vector).
 * @author Joshua McLean
 * @date 2012/10/08 13:20
 *
 * Construct a vertex of the given position, color, and normal vector.
 */
mace::Vertex::Vertex( 
    const mace::Vector3 &a_rPosition, 
    const mace::Color &a_rColor, // = mace::Color::BLACK
    const mace::Vector3 &a_rNormal // = mace::Vector3::UNIT_Y 
) : 
    position( a_rPosition ), 
    color( a_rColor )
{}


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Vertex::addTextureCoord( const Vector2 & )
 * @brief Add a texture coordinate to the vertex.
 * @param a_rCoord The coordinate to add as a texture coordinate.
 * @author Joshua McLean
 * @date 2012/10/08 13:21
 *
 * Check whether we are already at the maximum texture coordinates. If we are, return. Otherwise,
 * add the new texture coordinate to our vector of texture coordinates.
 */
void 
mace::Vertex::addTextureCoord( const mace::Vector2 &a_coord ) {
    if( m_textureCoord.size() >= MAX_TEXTURE_COORDS ) {
        boost::format formatter( "WARNING: New texture coords ignored: maximum %d allowed per "
            "vertex." );
        formatter % MAX_TEXTURE_COORDS;
        mace::Log::message( formatter.str() );

        return;
    }

    m_textureCoord.push_back( a_coord );
}


//
// PUBLIC MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Vertex::getTextureCoord( const uint32_t )
 * @brief Get a texture coordinate at the specified index.
 * @param a_index The index for the texture coordinate.
 * @return A pointer to the texture coordinate if the index is valid; else, nullptr.
 * @author Joshua McLean
 * @date 2012/10/08 13:22
 *
 * If the index is greater than the number of texture coordinates we have, return nullptr. 
 * Otherwise, return a pointer to the texture coordinate at the given index.
 */
mace::Vector2 * const
mace::Vertex::getTextureCoord( const uint32_t a_index ) {
    if( a_index >= m_textureCoord.size() ) return nullptr;
    return &m_textureCoord[ a_index ];
}
