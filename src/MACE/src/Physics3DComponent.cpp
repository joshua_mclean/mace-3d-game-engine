#include "mace/Physics3DComponent.h"


//
// PUBLIC CTOR
//

/**
 * @fn Physics3dComponent::Physics3dComponent( const std::string & )
 * @brief Default constructor.
 * @param a_rName The name for this Physics 3D Component.
 * @author Joshua McLean
 * @date 2012/10/06 17:17
 *
 * Construct a Physics 3D Component with default values and the given name.
 */
mace::Physics3dComponent::Physics3dComponent( 
    const std::string &a_rName // = "" 
) : 
    Component( a_rName ), 
    m_center( mace::Vector3::ZERO ),
    m_mass( 1.0f ),
    m_rotation( Ogre::Quaternion() ),
    m_pSimulation( nullptr ),
    m_pTargetModel( nullptr ), 
    m_shapeType( Simulation::SHAPE_CUBE ),
    m_size( mace::Vector3::UNIT ),
    m_velocity( mace::Vector3::ZERO ),
    m_worldLimitMax( mace::Vector3( DEFAULT_WORLD_LIMIT ) ),
    m_worldLimitMin( mace::Vector3( -DEFAULT_WORLD_LIMIT ) )
{}


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Physics3dComponent::clone( const std::string & )
 * @brief Create a clone of this Physics 3D Component.
 * @param a_rCloneName The name for the cloned Physics 3D Component.
 * @return A pointer to the new clone.
 * @author Joshua McLean
 * @date 2012/10/06 17:24
 *
 * Create a deep clone with the given name.
 */
mace::Physics3dComponent * const
mace::Physics3dComponent::clone( 
    const std::string &a_rCloneName // = "" 
) const {
    auto pPhysicsComponent = new Physics3dComponent( *this );

    if( a_rCloneName != "" ) {
        pPhysicsComponent->setName( a_rCloneName );
    }

    return pPhysicsComponent;
}


//
// PUBLIC MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Physics3dComponent::getCenter()
 * @brief Get the center position of this Physics 3D Component.
 * @return A Vector3 containing the center position of this Physics 3D Component.
 * @author Joshua McLean
 * @date 2012/10/06 17:24
 *
 * If loaded, synchronize the center with Bullet Physics. Then, return the stored value for the 
 * center of this Physics 3D Component.
 */
mace::Vector3 
mace::Physics3dComponent::getCenter() { 
    if( isLoaded() ) {
        m_center = m_pSimulation->getRigidOrigin( getName() );
    }

    return m_center; 
}


/**
 * @fn Physics3dComponent::getRotation()
 * @brief Get the rotational value for this Physics 3D Component.
 * @return An OGRE Quaternion containing the rotational value for this Physics 3D Component.
 * @author Joshua McLean
 * @date 2012/10/06 17:26
 *
 * If loaded, synchronize the rotational value for with Bullet Physics. Then, return the stored 
 * value for the rotation.
 */
Ogre::Quaternion 
mace::Physics3dComponent::getRotation() { 
    if( isLoaded() ) {
        m_rotation = m_pSimulation->getRigidRotation3( getName() );
    }

    return m_rotation;
}

/**
 * @fn Physics3dComponent::getVelocity()
 * @brief Get the velocity.
 * @return A Vector3 containing the linear velocity.
 * @author Joshua McLean
 * @date 2012/10/06 17:27
 *
 * If loaded, synchronize the velocity with the linear velocity from Bullet Physics. Then, return 
 * the stored value for the velocity.
 */
mace::Vector3 
mace::Physics3dComponent::getVelocity() {
    if( isLoaded() ) {
        m_velocity = m_pSimulation->getRigidVelocity( getName() );
    }

    return m_velocity;
}


/**
 * @fn Physics3dComponent::getDataString()
 * @brief Get an information string.
 * @return A string containing data from this.
 * @author Joshua McLean
 * @date 2012/10/06 17:28
 *
 * Construct a string containing the name, mass, size, center position, and velocity, then return 
 * that string.
 */
std::string 
mace::Physics3dComponent::getDataString() {
    auto position = getCenter();
    auto velocity = getVelocity();

    boost::format fmt( 
        "[%s]\nMass %.2f\nSize %s\nCenter %s\nVelocity: %s" 
    );
    fmt % getName() % m_mass % m_size.toString( 2 ) % position.toString( 2 ) 
        % velocity.toString( 2 );
    return fmt.str();
}


//
// PUBLIC MEMBER FUNCTIONS - MUTATORS
//

/**
 * @fn Physics3dComponent::setMass( const float )
 * @brief Set the mass.
 * @param a_mass The desired mass.
 * @author Joshua McLean
 * @date 2012/10/06 17:29
 *
 * Set the mass to the given value.
 */
void
mace::Physics3dComponent::setMass( const float a_mass ) { 
    m_mass = a_mass;
}


/**
 * @fn Physics3dComponent::setRotation( const Ogre::Quaternion & )
 * @brief Set the rotation.
 * @param a_rRotation An OGRE Quaternion containing the desired rotation.
 * @author Joshua McLean
 * @date 2012/10/06 17:31
 *
 * Set the rotation to the given value. If loaded, update the rigid body in Bullet Physics with this
 * information.
 */
void
mace::Physics3dComponent::setRotation( const Ogre::Quaternion &a_rRotation ) {
    m_rotation = a_rRotation;

    if( isLoaded() ) {
        m_pSimulation->setRigidRotation( getName(), a_rRotation );
    }
}


/**
 * @fn Physics3dComponent::setShape( const Simulation::shape_t )
 * @brief Set the shape.
 * @param a_shapeType The desired type of shape.
 * @author Joshua McLean
 * @date 2012/10/06 17:32
 *
 * Set the shape type to the given value. If loaded, update the rigid body in Bullet Physics with 
 * this information.
 */
void 
mace::Physics3dComponent::setShape( const Simulation::shape_t a_shapeType ) {
    m_shapeType = a_shapeType;

    if( isLoaded() ) {
        m_pSimulation->setRigidShape( getName(), a_shapeType, m_size );
    }
}


/**
 * @fn Physics3dComponent::setSize( const mace::Vector3 & )
 * @brief Set the size.
 * @param a_rDimensions A Vector3 containing the desired size.
 * @author Joshua McLean
 * @date 2012/10/06 17:32
 *
 * Set the size to the given value. If loaded, update the rigid body in Bullet Physics with this
 * information.
 */
void 
mace::Physics3dComponent::setSize( const mace::Vector3 &a_rDimensions ) { 
    m_size = a_rDimensions;

    if( isLoaded() ) {
        m_pSimulation->setRigidShape( getName(), m_shapeType, a_rDimensions );
    }
}


/**
 * @fn Physics3dComponent::setTargetModel( Primitive3dComponent * const )
 * @brief Set the target model.
 * @param a_pModel A pointer to the desired target model.
 * @author Joshua McLean
 * @date 2012/10/06 17:33
 *
 * Set the target model to the given pointer, then update stored information to reflect the center,
 * size, and rotation of that target model.
 */
void 
mace::Physics3dComponent::setTargetModel( Primitive3dComponent * const a_pModel ) {
    m_pTargetModel = a_pModel;
    m_center = a_pModel->getCenter();
    m_rotation = a_pModel->getRotation();
    m_size = a_pModel->getSize();
}


/**
 * @fn Physics3dComponent::setVelocityX( const float )
 * @brief Set the X velocity.
 * @param a_x The desired X velocity.
 * @author Joshua McLean
 * @date 2012/10/06 17:33
 *
 * Retrieve the current velocity, then change its X value to reflect the desired X velocity. Then,
 * update the velocity.
 */
void 
mace::Physics3dComponent::setVelocityX( const float a_x ) {
    m_velocity = getVelocity();

    m_velocity.x = a_x;

    updateVelocity();
}


/**
 * @fn Physics3dComponent::setVelocityY( const float )
 * @brief Set the Y velocity.
 * @param a_y The desired Y velocity.
 * @author Joshua McLean
 * @date 2012/10/06 17:33
 *
 * Retrieve the current velocity, then change its Y value to reflect the desired Y velocity. Then,
 * update the velocity.
 */
void 
mace::Physics3dComponent::setVelocityY( const float a_y ) {
    m_velocity = getVelocity();

    m_velocity.y = a_y;

    updateVelocity();
}


/**
 * @fn Physics3dComponent::setVelocityZ( const float )
 * @brief Set the Z velocity.
 * @param a_z The desired Z velocity.
 * @author Joshua McLean
 * @date 2012/10/06 17:33
 *
 * Retrieve the current velocity, then change its Z value to reflect the desired Z velocity. Then,
 * update the velocity.
 */
void 
mace::Physics3dComponent::setVelocityZ( const float a_z ) {
    m_velocity = getVelocity();

    m_velocity.z = a_z;

    updateVelocity();
}


/**
 * @fn Physics3dComponent::setVelocity( const Vector3 & )
 * @brief Set the velocity.
 * @param a_rVelocity The desired X velocity.
 * @author Joshua McLean
 * @date 2012/10/06 17:33
 *
 * Retrieve the current velocity, then change its X value to reflect the desired X velocity. Then,
 * update the velocity.
 */
void 
mace::Physics3dComponent::setVelocity( const mace::Vector3 &a_rVelocity ) {
    m_velocity = a_rVelocity;
            
    updateVelocity();
}


//
// PROTECTED COPY CTOR
//

/**
 * @fn Physics3dComponent::Physics3dComponent( const Physics3dComponent & )
 * @brief Copy constructor.
 * @param a_rModel3dComponent The component to be used for copying.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Copy constructor which is protected so it can only be called by the clone member function.
*/
mace::Physics3dComponent::Physics3dComponent( 
    const mace::Physics3dComponent &a_rPhysicsComponent 
) :
    Component( a_rPhysicsComponent.getName() ), 
    m_center( mace::Vector3::ZERO ),
    m_mass( 1.0f ),
    m_rotation( Ogre::Quaternion() ),
    m_pSimulation( nullptr ),
    m_pTargetModel( nullptr ), 
    m_shapeType( Simulation::SHAPE_CUBE ),
    m_size( mace::Vector3::UNIT ),
    m_velocity( mace::Vector3::ZERO ),
    m_worldLimitMax( mace::Vector3( DEFAULT_WORLD_LIMIT ) ),
    m_worldLimitMin( mace::Vector3( -DEFAULT_WORLD_LIMIT ) )
{}


//
// PROTECTED MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Physics3dComponent::getTypeName()
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
*/
std::string
mace::Physics3dComponent::getTypeName() {
    return "Physics 3D Component";
}


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn Physics3dComponent::onLoad()
 * @brief Additional load functionality.
 * @author Joshua McLean
 * @date 2012/10/08 23:31
 *
 * Create a rigid body with parameters from this in the simulation system.
 */
void 
// [override]
mace::Physics3dComponent::onLoad() {

    // get pointer to the simulation system
    m_pSimulation = Component::findComponentOfType< Simulation >();

    m_pSimulation->addRigidBody( 
        getName(), 
        Simulation::SHAPE_CUBE,
        m_center,
        mace::Vector3( m_size ),
        m_mass 
    );
}


/**
 * @fn Physics3dComponent::onNameChanged( const std::string & )
 * @brief Additional name change functionality.
 * @param a_rNewName The new name for the component.
 * @author Joshua McLean
 * @date 2012/10/08 23:31
 *
 * If loaded, synchronize the new name with the target body in the simulation system.
 */
void 
// [override]
mace::Physics3dComponent::onNameChanged( const std::string &a_rNewName ) {
    if( isLoaded() ) {
        m_pSimulation->setRigidName( a_rNewName, getName() );
    }
}


/**
 * @fn Physics3dComponent::onUpdate( const float )
 * @brief Additional update functionality.
 * @param a_dtms The number of milliseconds that passed between the previous update cycle and this
 *   update cycle.
 * @author Joshua McLean
 * @date 2012/10/08 23:32
 *
 * If this component has a target model, update it to reflect any changes in the rigid body this
 * represents. Then, if we have left the world limits, destroy the owning entity.
 */
void 
// [override]
mace::Physics3dComponent::onUpdate( const float a_dtms ) {

    // if we have a target spatial component, sync its position with our physics object
    if( m_pTargetModel != nullptr ) {
        m_pTargetModel->setCenter( getCenter() );
        m_pTargetModel->setSize( m_size );
        m_pTargetModel->setRotation( getRotation() );
    }
    
    // if we're outside the world limits, get rid of us
    if( 
        m_center.x < m_worldLimitMin.x || m_center.x > m_worldLimitMax.x
        || m_center.y < m_worldLimitMin.y || m_center.y > m_worldLimitMax.y
        || m_center.z < m_worldLimitMin.z || m_center.z > m_worldLimitMax.z
    ) {
        getParentEntity()->markForDestroy();
    }
}


//
// PRIVATE MEMBER FUNCTIONS
//

/**
 * @fn Physics3dComponent::updateVelocity()
 * @brief Update the velocity for this body.
 * @author Joshua McLean
 * @date 2012/10/08 23:34
 *
 * If this is loaded, set the linear velocity for the matching rigid body in the simulation system
 * to match our linear velocity.
 */
void 
mace::Physics3dComponent::updateVelocity() {
    if( isLoaded() ) {
        m_pSimulation->setRigidLinearVelocity( getName(), m_velocity );
    }
}
