#include "mace/MoveZComponent.h"


//
// PUBLIC CTOR
//

/**
 * @fn MoveZComponent::MoveZComponent( const std::string & )
 * @brief Default constructor.
 * @param a_rName The name for the Move Z Component. Default is an empty string.
 * @author Joshua McLean
 * @date 2012/10/06 16:51
 *
 * Constructs a Move Z Component with the given name, or an empty name if none is specified.
 */
mace::MoveZComponent::MoveZComponent( 
    const std::string &a_rName // = ""
    ) :
MoveComponent( a_rName )
{}


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn MoveZComponent::clone( const std::string & )
 * @brief Create a clone of this Move Z Component.
 * @param a_rCloneName The name for the cloned Move Z Component.
 * @return A pointer to the new clone.
 * @author Joshua McLean
 * @date 2012/10/06 16:52
 *
 * Creates a deep clone of this Move Z Component.
 */
mace::MoveZComponent * const 
mace::MoveZComponent::clone( 
    const std::string &a_rCloneName // = ""
) const {
    auto pMoveZComp = new MoveZComponent( *this );

    if( a_rCloneName != "" ) {
        pMoveZComp->setName( a_rCloneName );
    }

    return pMoveZComp;
}


//
// PUBLIC MEMBER FUNCTIONS - ACTIONS
//


/**
 * @fn MoveZComponent::moveEnd()
 * @brief Ends the move for this Move Z Component.
 * @author Joshua McLean
 * @date 2012/10/06 16:52
 *
 * Ensures that the target physics exists and then tells it to nullify its velocity along the Z 
 * axis.
 */
void
// [override]
mace::MoveZComponent::moveEnd() {
    checkPhysicsExists();
    m_pPhysicsComponent->setVelocityZ( 0.0f );
}


/**
 * @fn MoveZComponent::moveNegativeHeld()
 * @brief Called when the negative movement button is held.
 * @author Joshua McLean
 * @date 2012/10/06 16:53
 *
 * Ensures that the target physics exists and then tells it to move negatively along the Z axis.
 */
void 
// [override]
mace::MoveZComponent::moveNegativeHeld() {
    checkPhysicsExists();
    m_pPhysicsComponent->setVelocityZ( -MOVE_SPEED );
}


/**
 * @fn MoveZComponent::movePositiveHeld()
 * @brief Called when the positive movement button is held.
 * @author Joshua McLean
 * @date 2012/10/06 16:53
 *
 * Ensures that the target physics exists and then tells it to move positively along the Z axis.
 */
void 
// [override]
mace::MoveZComponent::movePositiveHeld() {
    checkPhysicsExists();
    m_pPhysicsComponent->setVelocityZ( MOVE_SPEED );
}


//
// PROTECTED COPY CTOR
//

/**
 * @fn MoveZComponent::MoveZComponent( const MoveZComponent & )
 * @brief Copy constructor.
 * @param a_rMoveXComp The Move Z Component to be copied.
 * @author Joshua McLean
 * @date 2012/10/06 16:54
 *
 * Private copy constructor which is only callable through the deep clone function.
 */
mace::MoveZComponent::MoveZComponent( const mace::MoveZComponent &a_rMoveZComp ) :
MoveComponent( a_rMoveZComp.getName() )
{}


//
// PROTECTED MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn MoveZComponent::getTypeName()
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string 
// [override]
mace::MoveZComponent::getTypeName() {
    return "Move Z Component";
}


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn MoveZComponent::initControls()
 * @brief Initialize controls for this Move Z Component.
 * @author Joshua McLean
 * @date 2012/10/06 16:54
 *
 * Binds the down and up arrow keys to moving negatively and positively, respectively, along the
 * Y axis.
 */
void
// [override]
mace::MoveZComponent::initControls() {
    auto pInputSystem = Component::findComponentOfType< Input >();

    Input::CommandFunc backFunc = std::bind( &MoveZComponent::moveNegativeHeld, this );
    pInputSystem->addCommand( backFunc, "MoveBack", Input::HELD, OIS::KC_UP );

    Input::CommandFunc forwardFunc = std::bind( &MoveZComponent::movePositiveHeld, this );
    pInputSystem->addCommand( forwardFunc, "MoveForward", Input::HELD, OIS::KC_DOWN );
}
