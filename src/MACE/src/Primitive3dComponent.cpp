#include "mace/Primitive3dComponent.h"


//
// PUBLIC MEMBER FUNCTIONS - CTOR / DTOR
//

/**
 * @fn Primitive3dComponent::Primitive3dComponent
 * @brief Constructor.
 * @param a_rName The name for the new component.
 * @param a_type The type of the new component. SHAPE_CUBE is currently the only valid option.
 * @author Joshua McLean
 * @date 2012/10/08 12:19
 *
 * Constructs a Primitive 3D Component of the given name and type. The type specifies which shape
 * will be used as the basis for the component.
 */
mace::Primitive3dComponent::Primitive3dComponent( 
    const std::string &a_rName,
    const mace::Graphics::primitive3d_t a_type
) :
    RenderComponent( a_rName ),
    m_pGraphics( nullptr ),
    m_type( a_type )
{} // mace::Primitive3dComponent::Primitive3dComponent( const std::string &a_rName,
    // const mace::Graphics::primitive3d_t a_type )


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Primitive3dComponent::clone( const std::string & )
 * @brief Clone this.
 * @param a_rCloneName The name for the new clone.
 * @return A pointer to the newly created clone.
 * @author Joshua McLean
 * @date 2012/10/08 12:24
 *
 * Construct a deep clone of this component.
 */
mace::Primitive3dComponent * const 
mace::Primitive3dComponent::clone( 
    const std::string &a_rCloneName // = "" 
) const {
    auto pPrimitive3dComp = new Primitive3dComponent( *this );

    if( a_rCloneName != "" ) {
        pPrimitive3dComp->setName( a_rCloneName );
    }

    return pPrimitive3dComp;
} // mace::Primitive3dComponent * const mace::Primitive3dComponent::clone( 
    // const std::string &a_rCloneName // = "" ) const


//
// PUBLIC MEMBER FUNCTIONS - MUTATORS
//

/**
 * @fn Primitive3dComponent::setCenter( const Vector3 & )
 * @brief Set the center.
 * @param a_rCenter The desired center for the component.
 * @author Joshua McLean
 * @date 2012/10/08 12:25
 *
 * Pass the desired center to the base Spatial Component. Then, if loaded, update the internal OGRE
 * primitive's center to match the new center.
 */
void 
mace::Primitive3dComponent::setCenter( const mace::Vector3 &a_rCenter ) { 
    SpatialComponent::setCenter( a_rCenter );

    if( isLoaded() ) {
        m_pGraphics->setEntityCenter( getName(), getCenter() );
    }
} // void mace::Primitive3dComponent::setCenter( const mace::Vector3 &a_rCenter )


/**
 * @fn Primitive3dComponent::setMaterialName( const std::string & )
 * @brief Set the material name.
 * @param a_rName The new material name.
 * @author Joshua McLean
 * @date 2012/10/08 12:25
 *
 * Pass the desired material name to the base Render Component. Then, if loaded, update the 
 * internal OGRE primitive's material name.
 */
void 
mace::Primitive3dComponent::setMaterialName( const std::string &a_rName ) {
    RenderComponent::setMaterialName( a_rName );

    // if we're loaded, update the graphics entity
    if( isLoaded() ) {
        m_pGraphics->setEntityMaterialName( getName(), a_rName );
    }
} // void mace::Primitive3dComponent::setMaterialName( const std::string &a_rName )


/**
 * @fn Primitive3dComponent::setRotation( const Ogre::Quaternion & )
 * @brief Set the rotation.
 * @param a_rRotation The desired rotation for the component.
 * @author Joshua McLean
 * @date 2012/10/08 12:25
 *
 * Pass the desired rotation to the base Render Component. Then, if loaded, update the internal OGRE
 * primitive's rotation.
 */
void 
mace::Primitive3dComponent::setRotation( const Ogre::Quaternion &a_rRotation ) {
    RenderComponent::setRotation( a_rRotation );

    if( isLoaded() ) {
        m_pGraphics->setEntityRotation( getName(), a_rRotation );
    }
} // void mace::Primitive3dComponent::setRotation( const Ogre::Quaternion &a_rRotation )



/**
 * @fn Primitive3dComponent::setSize( const mace::Vector3 & )
 * @brief Set the size.
 * @param a_rSize The desired size for the component.
 * @author Joshua McLean
 * @date 2012/10/08 12:25
 *
 * Pass the desired size to the base Render Component. Then, if loaded, update the internal OGRE
 * primitive's size.
 */
void 
mace::Primitive3dComponent::setSize( const mace::Vector3 &a_rSize ) {
    RenderComponent::setSize( a_rSize );

    if( isLoaded() ) {
        m_pGraphics->setEntitySize( getName(), a_rSize );
    }
} // void mace::Primitive3dComponent::setSize( const mace::Vector3 &a_rSize )


//
// PROTECTED COPY CTOR
//

/**
 * @fn Primitive3dComponent::Primitive3dComponent( const Primitive3dComponent & )
 * @brief Copy constructor.
 * @param a_rModel3dComponent The component to be used for copying.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Copy constructor which is protected so it can only be called by the clone member function.
*/
mace::Primitive3dComponent::Primitive3dComponent( const Primitive3dComponent &a_rModel3dComponent) : 
    RenderComponent( a_rModel3dComponent.getName() ),
    m_type( a_rModel3dComponent.m_type )
{} // mace::Primitive3dComponent::Primitive3dComponent( 
    // const Primitive3dComponent &a_rModel3dComponent) 


//
// PROTECTED MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Primitive3dComponent::getTypeName()
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string 
// [override]
mace::Primitive3dComponent::getTypeName() {
    return "Primitive 3D Component";
} // std::string mace::Primitive3dComponent::getTypeName()


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn Primitive3dComponent::onLoad()
 * @brief Additional loading functionality.
 * @author Joshua McLean
 * @date 2012/10/08 12:19
 *
 * Find the graphics manager and add a graphics entity with all the properties of this component.
 */
void
// [override]
mace::Primitive3dComponent::onLoad() {

    // get a pointer to the graphics manager
    m_pGraphics = Component::findComponentOfType< Graphics >();

    m_pGraphics->addGfxEntity( 
        Graphics::SHAPE_STR[ m_type ], 
        getMaterialName(),
        getName(), 
        getCenter() 
    );
} // void mace::Primitive3dComponent::onLoad() {
