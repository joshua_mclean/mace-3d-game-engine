#include "mace/mace.h"


const bool ASK_FOR_MATERIAL = false;
const char *const AUTO_MATERIAL = "test/fire";


/**
 * @fn mace::execute( Game * const )
 * @brief Execute the given game.
 * @param a_pGame A pointer to the game to be executed.
 * @author Joshua McLean
 * @date 2012/10/08 23:42
 *
 * Set up controls for the game. Then, enter the main loop, updating the game as quickly as 
 * possible, until the window is closed. Finally, clean up after the game.
 */
void 
mace::execute( Game * const a_pGame ) {
    {
        boost::format fmt( "Executing game %s" );
        fmt % a_pGame->getFullName();
        Log::message( fmt.str() );
    }

    auto pGraphics = Component::findComponentOfType< Graphics >();
    auto pWindow = pGraphics->getWindow();

    // set up commands

    auto inputSystem = Component::findComponentOfType< Input >();

    Input::CommandFunc exitFunc = exit;
    inputSystem->addCommand( exitFunc, "Game:Exit", Input::PRESSED, OIS::KC_ESCAPE );

    Input::CommandFunc fullscreenFunc = toggleFullscreen;
    inputSystem->addCommand( fullscreenFunc, "Game:ToggleFullscreen", Input::PRESSED, 
        OIS::KC_NUMPADENTER );

    int numUpdates = 0;
    Ogre::Timer gameTimer;

    // main loop
    do {

        // update the game
        a_pGame->update();

        ++numUpdates;
    } while( !pWindow->isClosed() );    

    {
        boost::format fmt( "Shutting down game %s" );
        fmt % a_pGame->getFullName();
        Log::message( fmt.str() );
    }

    // cleanup
    a_pGame->destroy();
    a_pGame->unload();

    // ensure that all components have been cleaned up
    Component::eraseAllComponents();
}


/**
 * @fn mace::exit()
 * @brief Exit the game.
 * @author Joshua McLean
 * @date 2012/10/08 23:45
 *
 * Find the graphics manager. Get the window from the graphics manager and destroy it.
 */
void 
mace::exit() { 
    auto pGraphics = Component::findComponentOfType< Graphics >();
    pGraphics->getWindow()->destroy(); 
}


/**
 * @fn mace::toggleFullScreen
 * @brief Toggle full screen.
 * @author Joshua McLean
 * @date 2012/10/08 23:46
 *
 * Get the window from the graphics manager, then reverse its full screen value. Set the new
 * resolution based on whether it is full screen, then recalculate the ratio to adjust the camera's
 * aspect ratio accordingly.
 */
void 
mace::toggleFullscreen() {
    auto pGraphics = Component::findComponentOfType< Graphics >();
    auto pWindow = pGraphics->getWindow();

    bool goFullScreen = !pWindow->isFullScreen();
    uint32_t width = goFullScreen ? 1920 : 1024;
    uint32_t height = goFullScreen ? 1080 : 768;
    float ratio = static_cast< float >( width ) / static_cast< float >( height );

    // toggle the full screen flag and set window size
    pWindow->setFullscreen( goFullScreen, width, height );

    // set the camera's aspect ratio
    auto *camera = static_cast< Ogre::Camera * > (
        pGraphics->getSceneNode( "CAMERA" )->getAttachedObject( "CAMERA" )
    );
    camera->setAspectRatio( ratio );
}
