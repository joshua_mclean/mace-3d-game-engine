#include "mace/common.h"


/**
 * @fn mace::checkInit( const std::string &, void *, const std::string & )
 * @brief Check whether the given data has been allocated.
 * @param a_rName The name of the data to check.
 * @param a_pInitiable A pointer to the address of the data.
 * @param a_rSource The source which is checking the data, usually the containing function's name.
 * @author Joshua McLean
 * @date 2012/10/09 0:05
 *
 * If the value is not nullptr, return. Otherwise, thrown an exception since the memory has not been
 * initialized.
 */
void 
mace::checkInit( const std::string &a_rName, void *a_pInitable, const std::string &a_rSource ) {
    if( a_pInitable != nullptr ) return;

    boost::format fmt( "ERROR: %s used before initialization." );
    fmt % a_rName;
    throw mace::Exception( mace::Exception::INITIALIZATION_ERROR, fmt.str(), a_rSource );
}
