#include "mace/Stage.h"


//
// CTOR / DTOR
//

/**
 * @fn Stage::Stage( const std::string & )
 * @brief Constructor.
 * @param a_rName The name (handle) for this Stage.
 * @author Joshua McLean
 * @date 2012/06/24 21:17
 *
 * Construct a Stage with the given name.
 */
mace::Stage::Stage( 
    const std::string &a_rName 
) : Entity( a_rName )
{
} // mace::Stage::Stage( const std::string &a_rName )


/**
 * @fn Stage::~Stage()
 * @brief Destructor.
 * @author Joshua McLean
 * @date 2012/06/24 21:17
 *
 * When the Stage is deleted, it executes the underlying Entity::~Entity() destructor.
 */
mace::Stage::~Stage() {
} // mace::Stage::~Stage()


//
// PUBLIC MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Stage::getTypeName()
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string 
// [override]
getTypeName() {
    return "Stage";
} // std::string getTypeName()


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Stage::addEntity( Entity * const )
 * @brief Add an Entity to the Stage.
 * @param a_pEntity A pointer to the Entity to be added.
 * @author Joshua McLean
 * @date 2012/06/24 21:18
 *
 * Simply a wrapper for Entity::addComponent( Component * const ) which clarifies adding an Entity.
 */
void 
mace::Stage::addEntity( mace::Entity * const a_pEntity ) {
    addComponent( a_pEntity );
} // void mace::Stage::addEntity( mace::Entity * const a_pEntity )
