#include "mace/Game.h"


//
// PUBLIC DTOR
//

/**
 * @fn Game::~Game()
 * @brief Destructor.
 * @author Joshua McLean
 * @date 2012/06/24 20:44
 *
 * If there is a current stage, delete it.
 */
mace::Game::~Game() {
    Log::close();
} // mace::Game::~Game()


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Game::getStagePtr() const
 * @brief Get a pointer to the current Stage.
 * @return A pointer to the current Stage.
 * @author Joshua McLean
 * @date 2012/06/24 20:53
 *
 * Return a pointer to the current Stage, which is nullptr if there is no current Stage.
 */
mace::Stage * const 
mace::Game::getStagePtr() const {
    return m_pStage; 
} // mace::Stage * const mace::Game::getCurrentStagePtr() const


// systems must be initialized in a specific order, so are manually created/registered here
/*
 * Add core system Component objects to the Game. Create and register the game recursively, which
 * automatically creates, loads, and registers all attached game systems.
*/
void 
mace::Game::addDefaultSystems() {
    auto pGraphics = new Graphics( "default graphics system" );
    addComponent( pGraphics );

    auto pInput = new Input( "default input system" );
    addComponent( pInput );

    auto pSimulation = new Simulation( "default simulation system" );
    addComponent( pSimulation );
} // void mace::Game::addDefaultSystems


/**
 * @fn Game::replaceStage( Stage * const, const bool )
 * @brief Load a Stage into the Game.
 * @return A pointer to the previous Stage, provided for the user to dispose of as they wish.
 * @author Joshua McLean
 * @date 2012/06/24 20:55
 *
 * Before loading the stage, ensure the game is loaded. If we currently have a stage, deregister 
 * and destroy it. (Note: The old Stage is not deleted from memory, as the user may want to reuse
 * the Stage. For this reason, a pointer to it is returned at the end of this function.) Set the 
 * new Stage. Add it as a Component to the Game, then create and register the new Stage. Finally,
 * return a pointer to the old Stage.
 */
mace::Stage * const 
mace::Game::replaceStage( 
    mace::Stage * const a_pStage, 
    const bool a_destroy // = true
) {

    // if a stage is loaded but the game isn't yet loaded, throw exception
    if( !isLoaded() ) {
        throw mace::Exception(
            Exception::INITIALIZATION_ERROR, 
            "Game must be initialized before a stage is loaded.",
            "Game::loadStage()"
        );
    }

    Stage *pOldStage = m_pStage;

    if( 
        pOldStage != nullptr    // we have a stage
        && a_destroy            // we're told to destroy the stage
    ) {
        pOldStage->destroy();
        pOldStage->unload();
    }

    // attach new stage
    m_pStage = a_pStage;
    addComponent( m_pStage );

    return pOldStage;
} // mace::Stage * const mace::Game::replaceStage( mace::Stage * const a_pStage, 
    // const bool a_destroy )


//
// PROTECTED MEMBER FUNCTIONS - "EVENTS"
//


/**
 * @fn Game::onDestroy()
 * @brief Additional destroy functionality.
 * @author Joshua McLean
 * @date 2012/10/08 17:20
 *
 * Removes the stage from the game when it is destroyed. This effectively "resets" the game to its
 * non-existent state.
 */
void
// [override]
mace::Game::onDestroy() {

    // clear pointer to current stage
    if( m_pStage != nullptr ) {
        m_pStage = nullptr;
    }

    // run base destroy event
    Entity::onDestroy();
} // void mace::Game::onDestroy()


//
// PRIVATE CTOR
//

/**
 * @fn Game::Game( const std::string & )
 * @brief Constructor.
 * @author Joshua McLean
 * @date 2012/06/24 20:59
 *
 * This constructor is private in order to prevent instantiation of Game objects, since Game is a
 * singleton class. The instantiation of the game is done before anything else, so this is where we
 * display the legal information for the engine.
 *
 */
mace::Game::Game( const std::string &a_rName ) :
    Entity( a_rName ), 
    m_pStage( nullptr ) 
{
    // initialize the log
    Log::initialize( "game.log" );

    boost::format fmt( "%s\nPowered By %s version %s" );
    fmt % a_rName % MACE_ENGINE_TITLE % MACE_ENGINE_VER;
    auto engineCredits = fmt.str();

    int length = engineCredits.size();
    std::string borderStr = "";

    for( int count = 0; count < length; ++count ) {
        if( count % 2 == 0 ) {
            borderStr += "*";
        } else {
            borderStr += "~";
        }
    }

    Log::printToOutput( true );
    boost::format fmt2( "%s\n%s\n%s\n%s\n" );
    fmt2 % borderStr % engineCredits % MACE_ENGINE_COPYRIGHT % borderStr;
    Log::message( fmt2.str() );
    Log::printToOutput( false );
} // mace::Game::Game( const std::string &a_rName )
