#include "mace/Graphics.h"


//
// PUBLIC CTOR
//

/**
 * @fn Graphics::Graphics( const std::string & )
 * @brief Constructor.
 * @param a_rName The name for the graphics system.
 * @author Joshua McLean
 * @date 2012/10/08 19:08
 *
 * Construct a Graphics system with the given name.
 */
mace::Graphics::Graphics(
    const std::string &a_rName // = ""
) : 
    Component( a_rName ) 
{} // mace::Graphics::Graphics( const std::string &a_rName )


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Graphics::addGfxEntity( std::string, std::string, std::string, mace::Vector3 )
 * @brief Add a renderable entity to the OGRE scene.
 * @param a_meshName The name of the mesh to use.
 * @param a_materialName The name of the material to use.
 * @param a_entityName The name for the created entity, which will be used as a handle.
 * @param a_translation The initial translation from the center of the world for the entity.
 * @author Joshua McLean
 * @date 2012/10/08 19:50
 *
 * Get the scene manager and create an entity of the given name using the given mesh name. Set the
 * material to the given material, then translate to the given initial translation. Then, attach
 * the entity to a node and attach the node to OGRE's root node.
 */
void 
mace::Graphics::addGfxEntity( 
    std::string a_meshName, 
    std::string a_materialName,
    std::string a_entityName, 
    mace::Vector3 a_translation
) {
    auto pSceneManager = getSceneManager();

    // create an instance of the given mesh
    auto pOgreEntity = pSceneManager->createEntity( a_entityName, a_meshName );

    // set the material for the mesh
    pOgreEntity->setMaterialName( a_materialName );

    // set up the initial translation
    auto translation = a_translation.toOgreVector3();

    // wrap entity in a node so it can be added to the scene
    // note: it's OK for the entity and node to have the same name
    auto pRootNode = pSceneManager->getRootSceneNode();
    auto pSceneNode = pRootNode->createChildSceneNode( a_entityName, translation );

    // add the node to the scene so it updates
    pSceneNode->attachObject( pOgreEntity );
} // void mace::Graphics::addGfxEntity( std::string a_meshName, std::string a_materialName,
    // std::string a_entityName, mace::Vector3 a_translation )


/**
 * @fn Graphics::addTextureCoord( mace::Vector2, uint32_t )
 * @brief Add a texture coordinate to the given vertex.
 * @param a_textureCoord The texture coordinate to be added.
 * @param a_vertIndex The index of the vertex to which the coordinate will be added.
 * @author Joshua McLean
 * @date 2012/10/08 19:57
 *
 * Return immediately if we have no vertexes. If no vertex index was specified, we add the texture
 * coordinate to the most recently added vertex; otherwise, ensure that the given vertex index is 
 * within range. If it isn't, return. Otherwise, find the vertex at that index and add the given
 * texture coordinate.
 */
void 
mace::Graphics::addTextureCoord( mace::Vector2 a_textureCoord, uint32_t a_vertIndex ) {

    // if we don't have vertexes, can't do this
    uint32_t numVerts = m_vertexList.size();
    if( numVerts == 0 ) {
        Log::message( "[GFX] WARNING: Tried to add tex coord, but no verts exist." );
        return;
    }

    // if no vertex index is specified, add to the most recent vertex
    if( a_vertIndex == UINT32_MAX ) {
        m_vertexList[ numVerts - 1 ].addTextureCoord( a_textureCoord );
    } else {

        // index out of range
        if( a_vertIndex > numVerts ) {
            boost::format formatter( 
                "[GFX] WARNING: Tried to add tex coord to vert index out of range ( index %d in set %d)."
            );
            formatter % a_vertIndex;
            formatter % numVerts;
            mace::Log::message( formatter.str() );
            return;
        }

        // add to the specified vertex
        m_vertexList[ a_vertIndex ].addTextureCoord( a_textureCoord );
    }
} // void mace::Graphics::addTextureCoord( mace::Vector2 a_textureCoord, uint32_t a_vertIndex )


/**
 * @fn Graphics::addTriangle( uint32_t, uint32_t, uint32_t )
 * @brief Add a triangle tot he triangle list.
 * @param a_index1 The first index.
 * @param a_index2 The second index.
 * @param a_index3 The third index.
 * @author Joshua McLean
 * @date 2012/10/08 20:00
 *
 * Construct a triangle with the given indices and push it into the triangle list.
 */
void 
mace::Graphics::addTriangle( uint32_t a_index1, uint32_t a_index2, uint32_t a_index3 ) {
    m_triangleList.push_back( Triangle( a_index1, a_index2, a_index3 ) );
} // void mace::Graphics::addTriangle( uint32_t a_index1, uint32_t a_index2, uint32_t a_index3 )


/**
 * @fn Graphics::addVertex( Vertex )
 * @brief Add a vertex to the vertex list.
 * @param a_vertex The vertex to add.
 * @return The index of the newly added vertex.
 * @author Joshua McLean
 * @date 2012/10/08 20:01
 *
 * Push the vertex onto the vertex list, then return its index.
 */
uint32_t 
mace::Graphics::addVertex( Vertex a_vertex ) {
    m_vertexList.push_back( a_vertex );

    // return index to the vertex
    return m_vertexList.size() - 1;
} // uint32_t mace::Graphics::addVertex( Vertex a_vertex )


/**
 * @fn Graphics::clearGeometry()
 * @brief Clear the vertexes and triangles currently stored.
 * @author Joshua McLean
 * @date 2012/10/08 20:02
 *
 * Reset the vertex list and triangle list. This must be called to restart construction of a new
 * mesh or entity.
 */
void 
mace::Graphics::clearGeometry() {
    m_vertexList.clear();
    m_triangleList.clear();
} // void mace::Graphics::clearGeometry()


/**
 * @fn Graphics::createMesh( const std::string & )
 * @brief Create a mesh using the currently stored triangles and vertexes.
 * @author Joshua McLean
 * @date 2012/10/08 20:07
 *
 * Create a manual object from OGRE (an object that can be manually constructed using triangles).
 * Then, construct the manual object using the default material and information from the stored
 * vertexes. Create triangles using the triangle information, then convert the manual object to a
 * mesh (so it can be copied efficiently). Clear the geometry, since we won't be using it anymore,
 * and log that the mesh was created.
 */
void 
mace::Graphics::createMesh( const std::string &a_meshName ) {
    auto pSceneManager = getSceneManager();

    // create the manual object
    Ogre::String manualObjectName = a_meshName;
    manualObjectName.append( " manual object" );
    auto pManualObject = pSceneManager->createManualObject( manualObjectName );

    // the object is dynamic
    pManualObject->setDynamic( true );

    // construct the manual object
    pManualObject->begin( Graphics::DEFAULT_MATERIAL, Ogre::RenderOperation::OT_TRIANGLE_LIST );
    {
        // create vertexes for the object
        for( VertexIter iter = m_vertexList.begin(); iter != m_vertexList.end(); ++iter ) {

            // set vertex position
            pManualObject->position( iter->position.toOgreVector3() );

            // set vertex color
            pManualObject->colour( iter->color.toOgreColor() );

            // set vertex normal
            pManualObject->normal( iter->normal.toOgreVector3() );

            // set vertex texture coordinates
            for( uint32_t index = 0; index < MAX_TEXTURE_COORDS; ++index ) {
                auto pTextureCoord = iter->getTextureCoord( index );

                if( pTextureCoord == nullptr ) continue;
                pManualObject->textureCoord( pTextureCoord->toOgreVector2() );
            }
        }

        // create triangles for the object
        for( auto iter = m_triangleList.begin(); iter != m_triangleList.end(); ++iter ) {
                pManualObject->triangle( iter->index[ 0 ], iter->index[ 1 ], iter->index[ 2 ] );
        }
    }
    pManualObject->end();

    // convert the object to a mesh
    pManualObject->convertToMesh( a_meshName );

    // we're done with the geometry, so clear it
    clearGeometry();

    boost::format formatter( "[GFX] Created mesh %s" );
    formatter % a_meshName.c_str();
    mace::Log::message( formatter.str() );
} // void mace::Graphics::createMesh( const std::string &a_meshName )



/**
 * @fn Graphics::destroyEntity( const std::string & )
 * @brief Destroy the named OGRE entity.
 * @param a_rEntityName The name of the entity to destroy.
 * @author Joshua McLean
 * @date 2012/10/08 20:12
 *
 * Destroy the node containing the entity (which has the same name), which automatically destroys
 * the contained entity.
 */
void 
mace::Graphics::destroyEntity( const std::string &a_rEntityName ) {
    getSceneManager()->getRootSceneNode()->removeAndDestroyChild( a_rEntityName );
} // void mace::Graphics::destroyEntity( const std::string &a_rEntityName )


/**
 * @fn Graphics::rotateNode( const std::string &, const Vector3, const float )
 * @brief Rotate the given node along the given axis by the given angle.
 * @param a_rNodeName The name of the node to rotate.
 * @param a_axis The axis along which to rotate.
 * @param a_rads The angle to rotate the node in radians.
 * @author Joshua McLean
 * @date 2012/10/08 20:13
 *
 * Get the node of the given name, then rotate it along the given axis by the given angle.
 */
void 
mace::Graphics::rotateNode( 
    const std::string &a_rNodeName, 
    const mace::Vector3 a_axis,
    const float a_rads 
) {
    auto node = getSceneManager()->getSceneNode( a_rNodeName );
    node->rotate( a_axis.toOgreVector3(), Ogre::Radian( a_rads ) );
} // void mace::Graphics::rotateNode( const std::string &a_rNodeName, const mace::Vector3 a_axis,
    // const float a_rads )


/**
 * @fn Graphics::translateNode( const std::string &, const Vector3 & )
 * @brief Translate the given node by the given amount.
 * @param a_rNodeName The name of the node to translate.
 * @param a_rTranslation The amount to translate.
 * @author Joshua McLean
 * @date 2012/10/08 20:15
 *
 * Get the node matching the given name, then translate it by the given amount.
 */
void mace::Graphics::translateNode( 
    const std::string &a_rNodeName,
    const mace::Vector3 &a_rTranslation 
) {
    auto node = getSceneManager()->getSceneNode( a_rNodeName );
    node->translate( a_rTranslation.x, a_rTranslation.y, a_rTranslation.z );
} // void mace::Graphics::translateNode( const std::string &a_rNodeName,
    // const mace::Vector3 &a_rTranslation )



//
// PUBLIC MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Graphics::getEntity( const std::string & ) const
 * @brief Get the entity of the given name.
 * @param a_rName The name of the entity to look up.
 * @return A pointer to the entity.
 * @author Joshua McLean
 * @date 2012/10/08 20:16
 *
 * Get the entity of the given name from the OGRE scene manager and return it.
 */
Ogre::Entity * const 
mace::Graphics::getEntity( const std::string &a_rName ) const { 
    return getSceneManager()->getEntity( a_rName );
} // Ogre::Entity * const mace::Graphics::getEntity( const std::string &a_rName ) const 


/**
 * @fn Graphics::getRoot() const
 * @brief Get the OGRE root.
 * @return A pointer to the root node in OGRE.
 * @author Joshua McLean
 * @date 2012/10/08 20:18
 *
 * Check that OGRE's root has been initialized, then return it.
 */
Ogre::Root * const 
mace::Graphics::getRoot() const { 
    checkInit( "Ogre Root", m_pOgreRoot, "Ogre::getRoot()" );

    return m_pOgreRoot; 
} // Ogre::Root * const mace::Graphics::getRoot() const


/**
 * @fn Graphics::getSceneManager() const
 * @brief Get the OGRE scene manager.
 * @return A pointer to the OGRE scene manager.
 * @author Joshua McLean
 * @date 2012/10/08 20:18
 *
 * Check that the OGRE scene manager has been initialized, then return it.
 */
Ogre::SceneManager * const 
mace::Graphics::getSceneManager() const {
    checkInit( "Ogre Scene Manager", m_pSceneManager, "Ogre::getSceneManager()" );

    return m_pSceneManager; 
} // Ogre::SceneManager * const mace::Graphics::getSceneManager() const


/**
 * @fn Graphics::getSceneNode( const std::string & ) const
 * @brief Get the OGRE scene node matching the given name.
 * @param a_rName The name of the scene node.
 * @return A pointer to the OGRE scene node of the given name.
 * @author Joshua McLean
 * @date 2012/10/08 20:19
 *
 * Get the root scene node from the scene manager, then get its child of the given name. Cast the
 * child as a SceneNode and return it.
 */
Ogre::SceneNode * const 
mace::Graphics::getSceneNode( const std::string &a_rName ) const {
    return static_cast< Ogre::SceneNode *>
        ( getSceneManager()->getRootSceneNode()->getChild( a_rName ) );
} // Ogre::SceneNode * const mace::Graphics::getSceneNode( const std::string &a_rName ) const


/**
 * @fn Graphics::getCenter( const std::string & ) const
 * @brief Get the center of the specified scene node / entity.
 * @param a_rNodeName The name of the node to look up.
 * @return The center position of the given scene node.
 * @author Joshua McLean
 * @date 2012/10/08 20:20
 *
 * Get the scene node of the given name from the scene manager, then get its position. Return it,
 * converted to a MACE vector.
 */
mace::Vector3 
mace::Graphics::getCenter( const std::string &a_rNodeName ) const {
    auto position = getSceneManager()->getSceneNode( a_rNodeName )->getPosition();

    return mace::Vector3( position.x, position.y, position.z );
} // mace::Vector3 mace::Graphics::getCenter( const std::string &a_rNodeName ) const


/**
 * @fn Graphics::getWindow() const
 * @brief Get the OGRE window.
 * @return A pointer to the OGRE render window.
 * @author Joshua McLean
 * @date 2012/10/08 20:21
 *
 * Check that the OGRE render window has been initialized. Then, return it.
 */
Ogre::RenderWindow * const 
getWindow() const { 
    checkInit( "OGRE Window", m_pWindow, "Graphics::getWindow()" ); 
    return m_pWindow; 
} // Ogre::RenderWindow * const getWindow() const


/**
 * @fn Graphics::getTypeName()
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string 
// [override]
mace::Graphics::getTypeName() {
    return "Graphics";
} // std::string mace::Graphics::getTypeName()


//
// PUBLIC MEMBER FUNCTIONS - MUTATORS
//

/**
 * @fn Graphics::setEntityCenter( const std::string &, const Vector3 & )
 * @brief Set the center of the specified entity.
 * @param a_rEntityName The name of the entity to move.
 * @param a_rCenter The new center for the entity.
 * @author Joshua McLean
 * @date 2012/10/08 20:28
 *
 * Get the scene node with the given name, then set its position to the specified center value.
 */
void 
mace::Graphics::setEntityCenter( 
    const std::string &a_rEntityName, 
    const mace::Vector3 &a_rCenter 
) {
    getSceneNode( a_rEntityName )->setPosition( a_rCenter.x, a_rCenter.y, a_rCenter.z );
} // void mace::Graphics::setEntityCenter( const std::string &a_rEntityName, 
    // const mace::Vector3 &a_rCenter )


/**
 * @fn Graphics::setEntityMaterialName( const std::string &, const std::string & )
 * @brief Change the material used by the specified entity.
 * @param a_rEntityName The name of the entity.
 * @param a_rMaterialName The name for the new material.
 * @author Joshua McLean
 * @date 2012/10/08 20:30
 *
 * Get the entity of the given name, then set its material name to the new value.
 */
void 
mace::Graphics::setEntityMaterialName( 
    const std::string &a_rEntityName, 
    const std::string &a_rMaterialName 
) {
    auto pEntity = getEntity( a_rEntityName );
    pEntity->setMaterialName( a_rMaterialName );
} // void mace::Graphics::setEntityMaterialName( const std::string &a_rEntityName, 
    // const std::string &a_rMaterialName )


/**
 * @fn Graphics::setEntityRotation( const std::string &, const std::string & )
 * @brief Set the rotation of the specified entity.
 * @param a_rEntityName The name of the entity.
 * @param a_rRotation The new rotation for the entity.
 * @author Joshua McLean
 * @date 2012/10/08 20:32
 *
 * Get the entity of the given name, then set its orientation to the specified rotation.
 */
void 
mace::Graphics::setEntityRotation(
    const std::string &a_rEntityName, 
    const Ogre::Quaternion &a_rRotation
) {
    auto pNode = getSceneNode( a_rEntityName );
    pNode->setOrientation( a_rRotation );
} // void mace::Graphics::setEntityRotation( const std::string &a_rEntityName, 
    // const Ogre::Quaternion &a_rRotation ) 


/**
 * @fn Graphics::setEntitySize( const std::string &, const std::string & )
 * @brief Set the size of the specified entity.
 * @param a_rEntityName The name of the entity.
 * @param a_rSize The new size for the entity.
 * @author Joshua McLean
 * @date 2012/10/08 20:33
 *
 * Get the entity of the given name, then set its scale to the specified size.
 */
void mace::Graphics::setEntitySize( 
    const std::string &a_rEntityName, 
    const mace::Vector3 &a_rSize 
) {
    auto pNode = getSceneNode( a_rEntityName );
    pNode->setScale( a_rSize.toOgreVector3() );
} // void mace::Graphics::setEntitySize( const std::string &a_rEntityName, 
    // const mace::Vector3 &a_rSize )


//
// PUBLIC STATIC MEMBER DATA - CONSTANTS
//

const char *const mace::Graphics::DEFAULT_MATERIAL = "BaseWhiteNoLighting";
const char *const mace::Graphics::SHAPE_STR[ NUM_SHAPES ] = { "cube" };


//
// PROTECTED MEMBER FUNCTIONS
//


/**
 * @fn Graphics::onLoad()
 * @brief Additional load functionality.
 * @author Joshua McLean
 * @date 2012/10/08 20:33
 *
 * Initialize the OGRE engine and primitive shapes, then create the resource group manager, pointing
 * it toward the textures folder. Initialize the frame timer.
 */
void mace::Graphics::onLoad() {
    Log::message( "[GFX] Loading graphics manager: OGRE3D." );

    initEngine();
    initPrimitives();
        
    Log::message( "[OGRE] Loading resources." );

    Ogre::ResourceGroupManager *resGroupMan = Ogre::ResourceGroupManager::getSingletonPtr();

    // add textures to general group
    resGroupMan->addResourceLocation( "textures", "FileSystem", "General" );

    // load resources
    resGroupMan->initialiseAllResourceGroups();
    resGroupMan->loadResourceGroup( "General" );

    // create timer for frame limit
    m_pFrameTimer = new Ogre::Timer();
    m_pFrameTimer->reset();
} // void mace::Graphics::onLoad()


/**
 * @fn Graphics::onUpdate( const float )
 * @brief Update the graphics system.
 * @author Joshua McLean
 * @date 2012/10/08 20:34
 *
 * Check the timer to see if we have passed enough time to render a frame, then render one frame,
 * updating the window and swapping buffers. Reset the timer. Pump the Windows message events even
 * if we do not render a frame so that the window can react in real time.
 */
void mace::Graphics::onUpdate( const float a_dtms ) {

    // do the frame limited render
    uint32_t ms = m_pFrameTimer->getMilliseconds();
    if( ms > Graphics::MS_PER_FRAME ) {

        // update the window - don't swap buffers
        // for some reason, this also renders the next frame...
        m_pWindow->update( false );

        // for double buffering - wait for vsync
        m_pWindow->swapBuffers( true );
        m_pFrameTimer->reset();
    }

    // make sure we get windows messages (click etc.)
    Ogre::WindowEventUtilities::messagePump();
} // void mace::Graphics::onUpdate( const float a_dtms )


//
// PRIVATE MEMBER FUNCTIONS
//


/**
 * @fn Graphics::initCube()
 * @brief Create a cube rendering primitive.
 * @author Joshua McLean
 * @date 2012/10/08 20:36
 *
 * Construct a cube rendering primitive with a radius of 1.0f units using twelve triangles.
 */
void mace::Graphics::initCube() {
    boost::format formatter( "[GFX] Creating mesh for %s" );
    formatter % SHAPE_STR[ SHAPE_CUBE ];
    Log::message( formatter.str() );

    const auto SIZE = 1.0f;
    const auto MINUS_SIZE = -SIZE;
    const auto COLOR = mace::Color::TRANSPARENT_BLACK;

    // add vertexes with texture coordinates
    addVertex( mace::Vertex( 
        mace::Vector3( SIZE, MINUS_SIZE, SIZE ), 
        COLOR,
        mace::Vector3( 0.408248, -0.816497, 0.408248 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 1.0f, 0.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, MINUS_SIZE, -SIZE ), 
        COLOR,
        mace::Vector3( -0.408248, -0.816497, -0.408248 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 0.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( SIZE, MINUS_SIZE, -SIZE), 
        COLOR,
        mace::Vector3( 0.666667, -0.333333, -0.666667 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 1.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, MINUS_SIZE, SIZE ), 
        COLOR, 
        mace::Vector3( -0.666667, -0.333333, 0.666667 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 0.0f, 0.0f ) );

    addVertex( mace::Vertex(
        mace::Vector3( SIZE, SIZE, SIZE ), 
        COLOR,
        mace::Vector3( 0.666667, 0.333333, 0.666667 ) 
    ) );
    addTextureCoord( mace::Vector2( 1.0f, 0.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, MINUS_SIZE, SIZE ), 
        COLOR,
        mace::Vector3( -0.666667, -0.333333, 0.666667 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 0.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( SIZE, MINUS_SIZE, SIZE ), 
        COLOR,
        mace::Vector3( 0.408248, -0.816497, 0.408248 ) 
    ) );
    addTextureCoord( mace::Vector2( 1.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, SIZE, SIZE ), 
        COLOR,
        mace::Vector3( -0.408248, 0.816497, 0.408248 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 0.0f, 0.0f ) );

    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, SIZE, -SIZE ), 
        COLOR,
        mace::Vector3( -0.666667, 0.333333, -0.666667 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 0.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, MINUS_SIZE, -SIZE), 
        COLOR,
        mace::Vector3( -0.408248, -0.816497, -0.408248 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 1.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, MINUS_SIZE, SIZE ), 
        COLOR,
        mace::Vector3( -0.666667, -0.333333, 0.666667 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 1.0f, 0.0f ) );
    addVertex( 
        mace::Vertex( mace::Vector3( SIZE, MINUS_SIZE, -SIZE ), 
        COLOR,
        mace::Vector3( 0.666667, -0.333333, -0.666667 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 0.0f, 1.0f ) );

    addVertex( 
        mace::Vertex( mace::Vector3( SIZE, SIZE, -SIZE ), 
        COLOR, 
        mace::Vector3( 0.408248, 0.816497, -0.408248 ) 
    ) );
    addTextureCoord( mace::Vector2( 1.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( SIZE, MINUS_SIZE, SIZE ), 
        COLOR,
        mace::Vector3( 0.408248, -0.816497, 0.408248 ) 
    ) ); 
    addTextureCoord( mace::Vector2( 0.0f, 0.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( SIZE, MINUS_SIZE, -SIZE ), 
        COLOR,
        mace::Vector3( 0.666667, -0.333333, -0.666667 ) 
    ) );
    addTextureCoord( mace::Vector2( 1.0f, 0.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, MINUS_SIZE, -SIZE ), 
        COLOR,
        mace::Vector3( -0.408248, -0.816497, -0.408248 ) 
    ) );
    addTextureCoord( mace::Vector2( 0.0f, 0.0f ) );

    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, SIZE, SIZE ), 
        COLOR,
        mace::Vector3( -0.408248, 0.816497, 0.408248 ) 
    ) );
    addTextureCoord( mace::Vector2( 1.0f, 0.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( SIZE, SIZE, -SIZE ), 
        COLOR, 
        mace::Vector3( 0.408248, 0.816497, -0.408248 ) 
    ) );
    addTextureCoord( mace::Vector2( 0.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( MINUS_SIZE, SIZE, -SIZE ), 
        COLOR,
        mace::Vector3( -0.666667, 0.333333, -0.666667 ) 
    ) );
    addTextureCoord( mace::Vector2( 1.0f, 1.0f ) );
    addVertex( mace::Vertex( 
        mace::Vector3( SIZE, SIZE, SIZE ), 
        COLOR, 
        mace::Vector3( 0.666667, 0.333333, 0.666667 ) 
    ) );
    addTextureCoord( mace::Vector2( 0.0f, 0.0f ) );

    // add triangles
    addTriangle( 0, 1, 2 );
    addTriangle( 3, 1, 0 );
    addTriangle( 4, 5, 6 );
    addTriangle( 4, 7, 5 );
    addTriangle( 8, 9, 10 );
    addTriangle( 10, 7, 8 );
    addTriangle( 4, 11, 12 );
    addTriangle( 4, 13, 11 );
    addTriangle( 14, 8, 12 );
    addTriangle( 14, 15, 8 );
    addTriangle( 16, 17, 18 );
    addTriangle( 16, 19, 17 );

    // create the mesh
    createMesh( SHAPE_STR[ SHAPE_CUBE ] );
} // void mace::Graphics::initCube()


/**
 * @fn Graphics::initEngine()
 * @brief Initialize the OGRE API engine.
 * @author Joshua McLean
 * @date 2012/10/08 20:37
 *
 * Create the OGRE log file and initialize the root with the appropriate config files. Check for
 * an existing rendering system and try to use OpenGL if able. If there is no rendering system,
 * throw an exception. Create a window for OGRE and initialize it with default properties. Create
 * a scene manager and camera, then create a viewport to attach the window and camera. Finally,
 * set the window to active and automatically updated, and reset all OGRE timers.
 */
void mace::Graphics::initEngine() {
    mace::Log::message( "[GFX] Initializing rendering engine." );

    // initialize the root with a log file
    Ogre::String pluginFileName = "ogre_plugins.cfg";
    Ogre::String configFileName = "ogre_config.cfg";
    Ogre::String logFileName = "ogre.log";
    m_pOgreRoot = new Ogre::Root( pluginFileName, configFileName, logFileName );

    mace::Log::message( "[OGRE] Selected render engine: OGRE3D." );

    // check that we have a render system available
    const auto &renderSystemList = m_pOgreRoot->getAvailableRenderers();
    if( renderSystemList.size() == 0 ) {
        throw mace::Exception( mace::Exception::ILLEGAL_OPERATION, MSG_NO_RENDERER,
            "Graphics System" );
    } else if( renderSystemList.size() == 1 ) {

        // if we have only one render system, use it
        m_pOgreRoot->setRenderSystem( renderSystemList[ 0 ] );
    } else {

        // otherwise, set to OpenGL (which is typically faster)
        m_pOgreRoot->setRenderSystem( renderSystemList[ 1 ] );
    }

    // initialize the root
    {
        bool autoWindow = false;
        Ogre::String windowTitle = "";
        Ogre::String customCapabilities = "";
        m_pOgreRoot->initialise( autoWindow, windowTitle, customCapabilities );
    }

    // create the window
    Ogre::String windowTitle = "MACE";
    Ogre::uint32 width = 1024;
    Ogre::uint32 height = 768;
    Ogre::NameValuePairList params;
    params[ "FSAA" ] = "8";
    params[ "vsync" ] = true;
    m_pWindow = m_pOgreRoot->createRenderWindow( windowTitle, width, height, FULLSCREEN, 
        &params );

    // create the scene manager and camera
    m_pSceneManager = m_pOgreRoot->createSceneManager( Ogre::ST_GENERIC, "SCENE MANAGER" );
    auto pRootSceneNode = m_pSceneManager->getRootSceneNode();
    auto pCamera = m_pSceneManager->createCamera( "CAMERA");

    // create the camera and a node for the scene
    auto pCameraNode = pRootSceneNode->createChildSceneNode( "CAMERA" );
    pCameraNode->attachObject( pCamera );

    // create the viewport to match the size of the window
    auto viewportWidth = 1.0f;//0.88f;
    auto viewportHeight = 1.0f;//0.88f;
    auto viewportLeft = ( 1.0f - viewportWidth ) * 0.5f;
    auto viewportTop = ( 1.0f - viewportHeight ) * 0.5f;
    auto mainViewportZOrder = 100;
    auto pViewport = m_pWindow->addViewport( pCamera, mainViewportZOrder, viewportLeft, 
        viewportTop, viewportWidth, viewportHeight );

    // update the viewport when the window is updated
    pViewport->setAutoUpdated( true );

    // set the background to gray
    pViewport->setBackgroundColour( Ogre::ColourValue( 0.5f, 0.5f, 0.5f ) );

    // set camera properties
    float ratio = static_cast< float >( width ) / static_cast< float >( height );
    pCamera->setAspectRatio( ratio );

    // note: if ( far / near ) > 2000, clipping problems
    pCamera->setNearClipDistance( 1.5f );
    pCamera->setFarClipDistance( 3000.0f );

    m_pWindow->setActive( true );

    // we want control over when the window updates, in onUpdate()
    m_pWindow->setAutoUpdated( false );

    // clear any events that came in while we were creating the window
    m_pOgreRoot->clearEventTimes();
} // void mace::Graphics::initEngine()


/**
 * @fn Graphics::initPrimitives()
 * @brief Initialize all primitives (currently only cube).
 * @author Joshua McLean
 * @date 2012/10/08 20:39
 *
 * Log that we are loading primitives, then call initialization of the cube primitive.
 */
void mace::Graphics::initPrimitives() {
    mace::Log::message( "[GFX] Loading primitives." );

    initCube();
} // void mace::Graphics::initPrimitives()


//
// PRIVATE STATIC MEMBER DATA - CONSTANTS
//

const uint32_t mace::Graphics::FRAMES_PER_SECOND = 70;
const uint32_t mace::Graphics::MS_PER_FRAME = 1000 / mace::Graphics::FRAMES_PER_SECOND;
