#include "mace/MoveComponent.h"


//
// PUBLIC CTOR
//

/**
 * @fn MoveComponent::MoveComponent( const std::string & )
 * @brief Default constructor.
 * @param a_rName The name for this Move Component.
 * @author Joshua McLean
 * @date 2012/10/06 16:05
 *
 * Creates a Move Component which inherits Physics Component.
 */
mace::MoveComponent::MoveComponent( 
    const std::string &a_rName // = ""
) :
    Component( a_rName ),
    m_pPhysicsComponent( nullptr )
    {} // mace::MoveComponent::MoveComponent( const std::string &a_rName )


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn MoveComponent::clone( const std::string & )
 * @brief Disallow creating a clone of this Move Component.
 * @return a_rCloneName The name of the new clone to be created (ignored).
 * @return nullptr
 * @author Joshua McLean
 * @date 2012/10/06 16:06
 *
 * Returns nullptr as this is an abstract component and cannot be cloned.
 */
mace::MoveComponent * const 
mace::MoveComponent::clone( 
    const std::string &a_rCloneName // = ""
) const {

    // can't clone abstract component
    return nullptr;
} // mace::MoveComponent * const mace::MoveComponent::clone( const std::string &a_rCloneName ) const


//
// PUBLIC MEMBER FUNCTIONS - MUTATORS
//

/**
 * @fn MoveComponent::setTargetPhysics( Physics3dComponent * const )
 * @brief Set the target physics component.
 * @param a_target The Physics 3D Component which this Move Component will control.
 * @author Joshua McLean
 * @date 2012/10/06 16:07
 *
 * Sets a link to the target physics component controlled by this Move Component. This Component 
 * will be told to move according to the overridden controls.
 */
void 
mace::MoveComponent::setTargetPhysics( Physics3dComponent * const a_target ) {
    m_pPhysicsComponent = a_target;
} // void mace::MoveComponent::setTargetPhysics( Physics3dComponent * const a_target )



//
// PROTECTED MEMBER FUNCTIONS
//


/**
 * @fn MoveComponent::checkPhysicsExists()
 * @brief Check that the physics component exists.
 * @author Joshua McLean
 * @date 2012/10/06 16:07
 *
 * Ensures that the target physics component exists so that this Move Component does not attempt to
 * move a non-existent physics component.
 */
void 
mace::MoveComponent::checkPhysicsExists() {
    checkInit( 
        "Physics Target",
        m_pPhysicsComponent, 
        "MoveXComponent::checkPhysicsExists()" 
    );
} // void mace::MoveComponent::checkPhysicsExists()


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn MoveComponent::onCreate()
 * @brief Post-creation functionality.
 * @author Joshua McLean
 * @date 2012/10/06 16:08
 *
 * Calls the overridden function to initialize controls for this Move Component upon its creation.
 */
void 
// [override]
mace::MoveComponent::onCreate() {
    initControls();
} // void mace::MoveComponent::onCreate()
