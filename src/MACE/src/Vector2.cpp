#include "mace/Vector2.h"


//
// Operator Functions
//

/**
 * @fn Vector2::operator+( const Vector2 & ) const
 * @brief Add two vectors together.
 * @param a_rVec The vector to add to this one.
 * @param A vector representing the sum of the two vectors.
 * @author Joshua McLean
 * @date 2012/10/08 13:29
 *
 * If both vectors are points, throw an exception. Otherwise, sum all the parts into a new vector
 * and return that vector.
 */
mace::Vector2 mace::Vector2::operator+( const mace::Vector2 &a_rVec ) const {

    // point + point doesn't make sense
    if( m_isPoint && a_rVec.m_isPoint ) {
        throw std::exception( "Cannot add points together." );
    }
    
    Vector2 totalVec;
    totalVec.x = x + a_rVec.x;
    totalVec.y = y + a_rVec.y;
    totalVec.z = z + a_rVec.z;

    // if we succeeded in addition, the result is a vector
    totalVec.m_isPoint = false;

    return totalVec;
} // mace::Vector2 mace::Vector2::operator+( const mace::Vector2 &a_rVec ) const


/**
 * @fn Vector2::operator-() const
 * @brief Get the negative of this vector.
 * @return A vector containing the negative value of this vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:30
 *
 * Return a vector with negative parts.
 */
mace::Vector2 
mace::Vector2::operator-() const {
    return Vector2( -x, -y );
} // mace::Vector2 mace::Vector2::operator-() const


/**
 * @fn Vector2::operator-( const Vector2 & ) const
 * @brief Subtract a vector from this one.
 * @param a_rVec The vector to subtract.
 * @return A vector containing the value of this one minus the given vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:31
 *
 * Return the negative of the given vector plus this vector.
 */
mace::Vector2 
mace::Vector2::operator-( const mace::Vector2 &vec ) const {
    return -vec + *this;
} // mace::Vector2 mace::Vector2::operator-( const mace::Vector2 &vec ) const


/**
 * @fn Vector2::operator*( const float ) const
 * @brief Multiply this vector by a scalar value.
 * @param a_scale The scalar value.
 * @return A vector containing this vector scaled by the given value.
 * @author Joshua McLean
 * @date 2012/10/08 13:32
 *
 * Return a vector with this vector's parts multiplied by the scalar.
 */
mace::Vector2 
mace::Vector2::operator*( const float scale ) const {
    return Vector2( x * scale, y * scale );
} // mace::Vector2 mace::Vector2::operator*( const float scale ) const


/**
 * @fn Vector2::operator*=( const float )
 * @brief Multiply this vector by a scalar value and set it to the result.
 * @param a_scale The scalar value.
 * @return This vector after being multiplied by the scalar.
 * @author Joshua McLean
 * @date 2012/10/08 13:33
 *
 * Multiply this by the given scale, set this value to the given scale, then return this.
 */
mace::Vector2 
mace::Vector2::operator*=( const float scale ) {
    *this = *this * scale;

    return *this;
} // mace::Vector2 mace::Vector2::operator*=( const float scale )


/**
 * @fn Vector2::operator=( const Vector2 & )
 * @brief Set this vector equal to another vector.
 * @param a_rVec The vector whose values this vector will be set to.
 * @return This vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:34
 *
 * Set each component of this vector equal to the relevant component of the given vector, then
 * return this.
 */
mace::Vector2 
mace::Vector2::operator=( const mace::Vector2 &vec ) {
    x = vec.x;
    y = vec.y;

    return *this;
} // mace::Vector2 mace::Vector2::operator=( const mace::Vector2 &vec )


//
// Other Public Functions
//

/**
 * @fn Vector2::dot( const Vector2 & ) const
 * @brief Calculate the dot product of this vector with another.
 * @param a_vec The other vector.
 * @return The resulting dot product.
 * @author Joshua McLean
 * @date 2012/10/08 13:36
 *
 * If either vector is a point, throw an exception, as dot product can only be performed on a vector
 * with another vector. Calculate the dot product using the appropriate formula and return the 
 * result.
 */
float 
mace::Vector2::dot( const mace::Vector2 &vec ) const {
    if( m_isPoint || vec.m_isPoint ) {
        throw std::exception( "Cannot dot points or point with vector." );
    }

    return x * vec.x + y * vec.y;
} // float mace::Vector2::dot( const mace::Vector2 &vec ) const


/**
 * @fn Vector2::magnitude() const
 * @brief Calculate the magnitude of this vector.
 * @return A float containing the magnitude of this vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:37
 *
 * Calculate the magnitude of the vector and return it.
 */
float 
mace::Vector2::magnitude() const {
    return sqrt( x * x + y * y );
} // float mace::Vector2::magnitude() const


/**
 * @fn Vector2::normalize()
 * @brief Normalize this vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:38
 *
 * Get the magnitude of the vector and divide each component by it, creating the normalized vector.
 */
void 
mace::Vector2::normalize() {
    float mag = magnitude();

    x /= mag;
    y /= mag;
} // void mace::Vector2::normalize()


//
// Constants
//

const mace::Vector2 mace::Vector2::ZERO = mace::Vector2();
const mace::Vector2 mace::Vector2::UNIT = mace::Vector2( 1.0f );
const mace::Vector2 mace::Vector2::UNIT_X = mace::Vector2( 1.0f, 0.0f );
const mace::Vector2 mace::Vector2::UNIT_Y = mace::Vector2( 0.0f, 1.0f );
