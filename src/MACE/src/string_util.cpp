#include "mace/string_util.h"


/**
 * @fn mace::separateWords( const std::string & )
 * @brief Separate words in a given string at uppercase letters.
 * @param a_rString The string to be separated.
 * @return The separated string.
 * @author Joshua McLean
 * @date 2012/10/08 14:32
 *
 * Iterate through each letter of the string. If the letter is uppercase, put a space before it and
 * continue to the letter after it. Return the modified string when done. For instance, the string
 * "FooBarLollipop" will become "Foo Bar Lollipop" when returned.
 */
std::string 
mace::separateWords( const std::string &a_rString ) {
    std::string modifiedString = a_rString;

    for( auto iter = modifiedString.begin() + 1; iter != modifiedString.end(); ++iter ) {

        // if the letter is uppercase, insert a space before it then skip to the next character
        if( isupper( *iter ) ) {

            // if we have two uppercase letters in sequence, we're not in PascalCase so invalid
            if( iter + 1 != modifiedString.end() && isupper( *( iter + 1 ) ) ) {
                return a_rString;
            }

            modifiedString.insert( iter - 1, ' ' );
            ++iter;
        }
    }

    return modifiedString;
} // std::string mace::separateWords( const std::string &a_rString )


/**
 * @fn mace::strToUpper( const std::string & )
 * @brief Convert a string to uppercase.
 * @param a_rString The string to be converted.
 * @return The string in all uppercase.
 * @author Joshua McLean
 * @date 2012/10/08 14:33
 * 
 * This is a function which desperately needs to be in the standard library. Using toupper on each
 * individual character, convert the given string to uppercase. Return the resulting string.
 */
std::string 
mace::strToUpper( const std::string &a_rString ) {
    std::string upperString;

    for( uint32_t index = 0; index < a_rString.length(); ++index ) {
        upperString += toupper( a_rString[ index ] );
    }

    return upperString;
} // std::string mace::strToUpper( const std::string &a_rString )


/**
 * @fn mace::wordWrap( const std::string &, const uint32_t )
 * @brief Wrap words in a given string.
 * @param a_rString The string to be formatted.
 * @param a_lineCutoff The maximum length of a line, in characters (default 70).
 * @author Joshua McLean
 * @date 2012/10/08 14:34
 *
 * Reformat the given string to be cut off at the given number of characters at the nearest
 * whitespace, so that words are not split between lines. This is accomplished by jumping ahead in
 * the string by the given cutoff, then backtracking to the first whitespace and replacing it with
 * a newline. When a newline is discovered, the count is reset so that we do not duplicate newlines.
 * When done, return the formatted string.
 */
std::string 
mace::wordWrap( 
    const std::string &a_rString, 
    const uint32_t a_lineCutoff // = 70
) {
    std::string formattedString( a_rString );
    uint32_t count = 0;
    int32_t lastSpaceIndex = -1;

    for( uint32_t index = 0; index < formattedString.length(); ++index ) {
        ++count;

        char currentChar = formattedString[ index ];
        switch( currentChar ) {

            // if we hit a space, set the last space index
            case ' ': lastSpaceIndex = index; break;

            // if we hit a newline, reset the count (we're back to the left)
            case '\n': count = 0; break;
            default: break;
        }

        // if we're before the cutoff, process the next character
        if( count < a_lineCutoff ) {
            continue;
        }

        // if we're past the cutoff with no previous space, force a chop here
        if( lastSpaceIndex < 0 ) {
            formattedString.insert( index, "\n " );

        // otherwise, insert a \n before the last space we found and reset the count
        } else {
            formattedString.insert( lastSpaceIndex, "\n" );
            count = 0;
        }
    }

    return formattedString;
} // std::string mace::wordWrap( const std::string &a_rString, const uint32_t a_lineCutoff )
