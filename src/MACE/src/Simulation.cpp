\#include "mace/Simulation.h"


//
// PUBLIC CTOR / DTOR
//

/**
 * @fn Simulation::Simulation( const std::string & )
 * @brief Constructor.
 * @param a_rName The name of the Simulation system.
 * @author Joshua McLean
 * @date 2012/10/08 18:51
 *
 * Construct a Simulation system component with the given name.
 */
mace::Simulation::Simulation( 
    const std::string &a_rName // = ""
) : 
    Component( a_rName )
{} // mace::Simulation::Simulation( const std::string &a_rName )


/**
 * @fn Simulation::~Simulation()
 * @brief Destructor.
 * @author Joshua McLean
 * @date 2012/10/08 19:11
 *
 * Destroy all of the bodies that have been created with the Bullet Physics API, then delete all
 * of the dynamically allocated Bullet Physics objects this class used.
 */
mace::Simulation::~Simulation() {

    // clean up bodies
    while( !m_rigidBodyMap.isEmpty() ) {
        auto key = m_rigidBodyMap.getKeyAt( 0 );

        removeRigidBody( key );
    }

    delete m_broadphaseInterface;
    delete m_collisionConfig;
    delete m_collisionDispatcher;
    delete m_solver;
    delete m_world;
} // mace::Simulation::~Simulation()


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Simulation::addRigidBody( const std::string &, const shape_t, Vector3, Vector3, float )
 * @brief Add a rigid body to the Simulation system.
 * @param a_rName The name for the new rigid body, which will be used as a handle.
 * @param a_shape The shape for the rigid body, either SHAPE_CUBE or SHAPE_SPHERE.
 * @param a_origin The center origin for the rigid body.
 * @param a_dimensions The dimensions (size) of the rigid body.
 * @author Joshua McLean
 * @date 2012/10/08 19:12
 *
 * Create a shape for the rigid body, either a cube or sphere as specified. Construct a transform
 * which is the definition of the initial motion state, then use that to construct a rigid body
 * using Bullet Physics. Set the body's default values, then add it to the Bullet Physics
 * simulation. Finally, add the body to the Simulation component's map of rigid bodies.
 */
void 
mace::Simulation::addRigidBody( 
    const std::string &a_rName, 
    const shape_t a_shape,
    mace::Vector3 a_origin, 
    mace::Vector3 a_dimensions,
    float a_mass 
) {
    btCollisionShape *shape = nullptr;

    if( a_shape == SHAPE_CUBE ) {
        shape = new btBoxShape( a_dimensions.toBulletVector3() );
    } else if( a_shape == SHAPE_SPHERE ) {
        shape = new btSphereShape( 
            std::max( std::max( a_dimensions.x, a_dimensions.y ), a_dimensions.z ) 
        );
    }

    btVector3 localInertia( 0, 0, 0 );
    shape->calculateLocalInertia( a_mass, localInertia );

    btTransform initialTransform;

    initialTransform.setOrigin( a_origin.toBulletVector3() );
    initialTransform.setRotation( btQuaternion( 0, 0, 0, 1 ) );

    auto motionState = new btDefaultMotionState( initialTransform );

    auto body = new btRigidBody( 
        btRigidBody::btRigidBodyConstructionInfo (
            a_mass,
            motionState,
            shape,
            localInertia
        )
    );

    body->setSleepingThresholds( 0.01f, 0.01f );

    // default friction/restitution
    body->setFriction( 0.3f );
    body->setRestitution( 0.3f );

    m_world->addRigidBody( body );

    m_rigidBodyMap.add( a_rName, body );
} // void mace::Simulation::addRigidBody( const std::string &a_rName, const shape_t a_shape,
    // mace::Vector3 a_origin, mace::Vector3 a_dimensions, float a_mass )


/**
 * @fn Simulation::removeRigidBody
 * @brief Remove the specified rigid body from the simulation.
 * @param a_rName The name of the rigid body to remove.
 * @author Joshua McLean
 * @date 2012/10/08 19:16
 *
 * Get the rigid body by name, then remove it from both the Bullet Physics simulation and this
 * component's rigid body map. Then, delete the body.
 */
void 
mace::Simulation::removeRigidBody( const std::string &a_rName ) {
    auto body = getRigidBody( a_rName );

    m_world->removeRigidBody( body );
    m_rigidBodyMap.remove( a_rName );

    delete body;
} // void mace::Simulation::removeRigidBody( const std::string &a_rName )


//
// PUBLIC MEMBER FUNCTIONS - BODY ACCESSORS
//

/**
 * @fn Simulation::getRigidOrigin( const std::string & )
 * @brief Get the origin of the specified rigid body.
 * @param a_rName The name of the rigid body whose origin we want.
 * @return The origin of the specified rigid body.
 * @author Joshua McLean
 * @date 2012/10/08 19:17
 *
 * Find the body of the given name, then get its origin through its world transform. Return the
 * origin.
 */
mace::Vector3 
mace::Simulation::getRigidOrigin( const std::string &a_rName ) {
    auto body = getRigidBody( a_rName );
    auto origin = body->getWorldTransform().getOrigin();

    return mace::Vector3( origin );
} // mace::Vector3 mace::Simulation::getRigidOrigin( const std::string &a_rName )


/**
 * @fn Simulation::getRigidRotation3( const std:: string & )
 * @brief Get the three-dimensional rotation of the specified rigid body.
 * @param a_rName The name of the rigid body.
 * @return The rotation of the rigid body in three dimensions.
 * @author Joshua McLean
 * @date 2012/10/08 19:23
 *
 * Get the rigid body by name, then return a quaternion based on its rotation.
 */
Ogre::Quaternion 
mace::Simulation::getRigidRotation3( const std::string &a_rName ) {
    auto body = getRigidBody( a_rName );
    return toOgreQuaternion( body->getWorldTransform().getRotation() );
} // Ogre::Quaternion mace::Simulation::getRigidRotation3( const std::string &a_rName )


/**
 * @fn Simulation::getRigidVelocity( const std::string & )
 * @brief Get the linear velocity of the given rigid body.
 * @param a_rName The name of the rigid body.
 * @author Joshua McLean
 * @date 2012/10/08 19:24
 *
 * Find the rigid body by name, then return its linear velocity.
 */
mace::Vector3 
mace::Simulation::getRigidVelocity( const std::string &a_rName ) {
    return mace::Vector3( getRigidBody( a_rName )->getLinearVelocity() );
} // mace::Vector3 mace::Simulation::getRigidVelocity( const std::string &a_rName )


//
// PUBLIC MEMBER FUNCTIONS - BODY MUTATORS
//


/**
 * @fn Simulation::setGravity( const Vector3 )
 * @brief Set the gravity for the simulation.
 * @param a_rGravity The desired gravity in m/s^2.
 * @author Joshua McLean
 * @date 2012/10/08 19:26
 *
 * Set the gravity for the Bullet Physics world to the specified value.
 */
void 
mace::Simulation::setGravity( const mace::Vector3 &a_rGravity ) {
    m_world->setGravity( a_rGravity.toBulletVector3() );
} // void mace::Simulation::setGravity( const mace::Vector3 &a_rGravity )


/**
 * @fn Simulation::setRigidLinearVelocity( const std::string &, Vector3 & )
 * @brief Set the linear velocity of the named rigid body to the given value.
 * @param a_rName The name of the target rigid body.
 * @param a_rVelocity The desried velocity for the rigid body.
 * @author Joshua McLean
 * @date 2012/10/08 19:27
 *
 * 
 */
void 
mace::Simulation::setRigidLinearVelocity( const std::string &a_rName, Vector3 &a_rVelocity ) {
    auto body = getRigidBody( a_rName );
    btVector3 inertia;

    // set velocity
    body->setLinearVelocity( a_rVelocity.toBulletVector3() );
} // void mace::Simulation::setRigidLinearVelocity( const std::string &a_rName, 
    // Vector3 &a_rVelocity )


/**
 * @fn Simulation::setRigidName( const std::string &, const std::string & )
 * @brief Set the specified rigid body to a new name.
 * @param a_rPrevName The previous name for the rigid body.
 * @param a_rNewName The new name to label the rigid body.
 * @author Joshua McLean
 * @date 2012/10/08 19:31
 *
 * Find the specified rigid body, remove it from the rigid body map, then add it to the rigid body
 * map again with the new name.
 */
void 
mace::Simulation::setRigidName( const std::string &a_rPrevName, const std::string &a_rNewName ) {
    auto body = getRigidBody( a_rPrevName );

    // replace the entry for the body with the new name
    m_rigidBodyMap.remove( a_rPrevName );
    m_rigidBodyMap.add( a_rNewName, body );
} // void mace::Simulation::setRigidName( const std::string &a_rPrevName, 
    // const std::string &a_rNewName )


/**
 * @fn Simulation::setRigidRotation
 * @brief Set the rotation for the specified rigid body.
 * @param a_rName The name of the rigid body.
 * @param a_rRotation The new rotation for the rigid body.
 * @author Joshua McLean
 * @date 2012/10/08 19:33
 *
 * Find the rigid body of the given name and get its transform. Then, set the rotation of the
 * transform to the new value. Finally, reattach the transform to the body to update it.
 */
void 
mace::Simulation::setRigidRotation( const std::string &a_rName, const Ogre::Quaternion &a_rRotation ) {
    auto body = getRigidBody( a_rName );
    auto worldTransform = body->getWorldTransform();

    worldTransform.setRotation( toBulletQuaternion( a_rRotation ) );
    body->setWorldTransform( worldTransform );
} // void mace::Simulation::setRigidRotation( const std::string &a_rName, 
    // const Ogre::Quaternion &a_rRotation )


/**
 * @fn Simulation::setRigidShape
 * @brief Modify the shape of the specified rigid body, which may be the shape type and/or size.
 * @param a_rName The name of the target rigid body.
 * @param a_shapeType The desired shape for the rigid body.
 * @param a_rDimensions The desired size for the rigid body.
 * @author Joshua McLean
 * @date 2012/10/08 19:33
 *
 * Find the rigid body and create a new shape for it of the given type. Set the size of the shape
 * to match the given dimensions, then reattach the shape to the target body.
 */
void 
mace::Simulation::setRigidShape( 
    const std::string &a_rName, 
    const shape_t a_shapeType, 
    const mace::Vector3 &a_rDimensions 
) {
    auto body = getRigidBody( a_rName );

    btCollisionShape *shape = nullptr;

    if( a_shapeType == SHAPE_CUBE ) {
        shape = new btBoxShape( a_rDimensions.toBulletVector3() );
    } else if( a_shapeType == SHAPE_SPHERE ) {
        shape = new btSphereShape( 
            std::max( std::max( a_rDimensions.x, a_rDimensions.y ), a_rDimensions.z )
        );
    }

    body->setCollisionShape( shape );
} // void mace::Simulation::setRigidShape( const std::string &a_rName, const shape_t a_shapeType, 
    // const mace::Vector3 &a_rDimensions )


//
// PROTECTED MEMBER FUNCTIONS
//

/**
 * @fn Simulation::checkHasBody( const std::string & )
 * @brief Check that the rigid body map contains a body with the specified name.
 * @param a_rName The name of the rigid body to look up.
 * @author Joshua McLean
 * @date 2012/10/08 19:35
 *
 * If the desired rigid body is not found, throw an exception.
 */
void
mace::Simulation::checkHasBody( const std::string &a_rName ) {
    if( !m_rigidBodyMap.hasKey( a_rName ) ) {
        boost::format format( "Key '%s' does not refer to a simulation object." );
        format % a_rName;
        throw mace::Exception( 
            Exception::KEY_NOT_FOUND, 
            format.str(), 
            "Simulation::checkHasBody()" 
        );
    }
} // void mace::Simulation::checkHasBody( const std::string &a_rName )


//
// PROTECTED MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Simulation::getRigidBody( const std::string & )
 * @brief Get the rigid body of the specified name.
 * @param a_rName The name of the rigid body.
 * @return A pointer to the rigid body if found, otherwise throws an exception.
 * @author Joshua McLean
 * @date 2012/10/08 19:35
 *
 * Check that the desired body exists, then, if it does, return it.
 */
btRigidBody * const
mace::Simulation::getRigidBody( const std::string &a_rName ) {
    checkHasBody( a_rName );

    return m_rigidBodyMap.getValue( a_rName );
} // btRigidBody * const mace::Simulation::getRigidBody( const std::string &a_rName )


/**
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string 
// [override]
mace::Simulation::getTypeName() {
    return "Simulation";
} // std::string mace::Simulation::getTypeName()


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn Simulation::onCreate()
 * @brief Additional creation functionality.
 * @author Joshua McLean
 * @date 2012/10/08 19:39
 *
 * Create the broadphase interface, collision configuration and dispatcher, and constraint solver to
 * construct a world for use with Bullet Physics.
 */
void 
mace::Simulation::onCreate() {
    Log::message( "[SIM] Creating Bullet world instance." );

    m_broadphaseInterface = new btDbvtBroadphase();

    m_collisionConfig = new btDefaultCollisionConfiguration();
    m_collisionDispatcher = new btCollisionDispatcher( m_collisionConfig );

    m_solver = new btSequentialImpulseConstraintSolver;

    m_world = new btDiscreteDynamicsWorld( 
        m_collisionDispatcher, 
        m_broadphaseInterface, 
        m_solver, 
        m_collisionConfig 
    );
} // void mace::Simulation::onCreate()


/**
 * @fn Simulation::onUpdate( const float )
 * @brief Update the simulation.
 * @author Joshua McLean
 * @date 2012/10/08 19:40
 *
 * If we haven't spent any time away from the simulation, skip an update. Update only once every
 * 120th of a second. Tell the world to update with a calculated time step, maximum substeps,
 * and fixed time step.
 */
void
mace::Simulation::onUpdate( const float a_dtms ) {

    // if no time passed, don't bother updating
    if( a_dtms < std::numeric_limits< float >::epsilon() ) return;

    static float totalMs = 0;

    const float UPDATES_PER_SECOND = 120.0f;
    const float MS_PER_UPDATE = 1000.0f / UPDATES_PER_SECOND;

    totalMs += a_dtms;

    // update 120 times per second
    if( totalMs > MS_PER_UPDATE ) {

        // bullet takes seconds - MUST HAVE timeStep < maxSubSteps * fixedTimeStep
        float timeStep = totalMs * 0.001f;
        int maxSubSteps = 7;
        float fixedTimeStep = 0.016667f;

        float subStepTime = static_cast< float >( maxSubSteps ) * fixedTimeStep;

        m_world->stepSimulation( timeStep, maxSubSteps, fixedTimeStep );

        totalMs = 0.0f;
    }
} // void mace::Simulation::onUpdate( const float a_dtms )


//
// PRIVATE MEMBER FUNCTIONS
//

/**
 * @fn Simulation::toBulletQuaternion
 * @brief Convert an OGRE quaternion to a Bullet Physics quaternion.
 * @param a_rQuaternion The OGRE quaternion to convert.
 * @return A Bullet Physics quaternion equivalent to the given OGRE quaternion.
 * @author Joshua McLean
 * @date 2012/10/08 19:42
 *
 * Create a Bullet Physics quaternion with the same x, y, z, and w values of the OGRE quaternion,
 * then return it.
 */
btQuaternion
mace::Simulation::toBulletQuaternion( const Ogre::Quaternion &a_quaternion ) {
    return btQuaternion(
        a_quaternion.x,
        a_quaternion.y,
        a_quaternion.z,
        a_quaternion.w
    );
} // btQuaternion mace::Simulation::toBulletQuaternion( const Ogre::Quaternion &a_quaternion )


/**
 * @fn Simulation::toOgreQuaternion
 * @brief Convert an Bullet Physics quaternion to an OGRE quaternion.
 * @param a_rQuaternion The Bullet Physics quaternion to convert.
 * @return An OGRE quaternion equivalent to the given Bullet Physics quaternion.
 * @author Joshua McLean
 * @date 2012/10/08 19:42
 *
 * Create an OGRE quaternion with the same x, y, z, and w values of the Bullet Physics quaternion,
 * then return it.
 */
Ogre::Quaternion 
mace::Simulation::toOgreQuaternion( const btQuaternion &a_quaternion ) {
    return Ogre::Quaternion( 
        a_quaternion.getW(), 
        a_quaternion.getX(), 
        a_quaternion.getY(), 
        a_quaternion.getZ()
    );
} // Ogre::Quaternion mace::Simulation::toOgreQuaternion( const btQuaternion &a_quaternion )
