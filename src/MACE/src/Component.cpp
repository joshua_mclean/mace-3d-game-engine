#include "mace/Component.h"

//
// PUBLIC CTOR / DTOR / CLONE
//

/**
 * @fn Component::Component( const std::string & )
 * @brief Constructor.
 * @param a_name The name (handle) for the component. Must be unique.
 * @author Joshua McLean
 * @date 6/19/2012 12:28 PM
 *
 * Create a component with the given name (handle). This handle must be unique in order for the
 * lookup system to function properly.
 */
mace::Component::Component( 
    const std::string &a_rName // = "" 
) :
    m_isCreated( false ),
    m_isLoaded( false ),
    m_isMarkedForDestroy( false ),
    m_name( a_rName ),
    m_pParentEntity( nullptr ) 
{
    boost::format formatter( "Constructing '%s'" );
    formatter % getFullName();
    mace::Log::message( formatter.str() );
} // mace::Component::Component( const std::string name )


/**
 * @fn Component::~Component()
 * @brief Default destructor.
 * @author Joshua McLean
 * @date 2012/06/21 11:30
 *
 * Currently does nothing, but declared virtual to ensure inherited classes delete Component data.
 */
mace::Component::~Component() {
    boost::format formatter( "Destructing '%s'" );
    formatter % getName();
    mace::Log::message( formatter.str() );

    // if we're still attached to a parent, detach from it
    if( m_pParentEntity != nullptr ) {
        m_pParentEntity->removeComponent( this );
    }
} // mace::Component::~Component()


//
// PUBLIC MEMBER FUNCTIONS
//


/**
 * @fn Component::clone( const std::string & )
 * @brief Clone this.
 * @param a_rCloneName The name for the new clone.
 * @return A pointer to the newly created clone.
 * @author Joshua McLean
 * @date 2012/10/08 12:24
 *
 * Construct a deep clone of this component.
 */
mace::Component * const
mace::Component::clone( 
    const std::string &a_rCloneName // = "" 
) const {
    auto pComp = new Component( *this );

    if( a_rCloneName == "" ) {
        pComp->setName( pComp->getTypeName() );
    } else {
        pComp->setName( a_rCloneName );
    }

    boost::format fmt( "Created clone of '%s' called '%s'" );
    fmt % getName() % pComp->getFullName();
    mace::Log::message( fmt.str() );

    return pComp;
}


/**
 * @fn Component::create()
 * @brief Create this component.
 * @param a_recurse True to recurse through Component tree, false to create only this Component.
 *   Defaults to false.
 * @author Joshua McLean
 * @date 6/19/2012 12:49 AM
 *
 * If told to recurse, tell the next component to create. Run the top-level Component::onCreate()
 * function and Component::load().
 */
void 
mace::Component::create() {

    // don't create more than once
    if( isCreated() ) return;

    boost::format formatter( "Creating '%s'" );
    formatter % getFullName();
    mace::Log::message( formatter.str() );

    m_isCreated = true;

    // overridden to provide per-Component-type functionality
    onCreate();
} // void mace::Component::create()


/**
 * @fn Component::destroy()
 * @brief Destroy this component.
 * @param a_recurse True to recurse through Component tree, false to destroy only this Component.
 *   Defaults to false.
 * @author Joshua McLean
 * @date 6/19/2012 2:02 AM
 *
 * Remove this Component from its parent.
 */
void 
mace::Component::destroy() {

    // don't destroy if not loaded
    if( !isCreated() ) return;

    boost::format formatter( "Destroying '%s'" );
    formatter % getName();
    mace::Log::message( formatter.str() );

    m_isCreated = false;

    // overridden to provide per-Component-type functionality
    this->onDestroy();
} // void mace::Component::destroy()


/**
 * @fn Component::load()
 * @brief Load this Component.
 * @author Joshua McLean
 * @date 2012/06/21 13:54
 *
 * Call Component::onLoad() and flag this Component as loaded.
 */
void 
mace::Component::load() { 

    // don't load if already loaded
    if( isLoaded() ) return;

    boost::format formatter( "Loading '%s'" );
    formatter % getFullName();
    mace::Log::message( formatter.str() );

    // make sure this Component has a name
    if( m_name == "" ) {
        setName( getTypeName() );
    } else {
        setName( getName() );
    }

    doRegister( getName() );

    // overridden to provide per-Component-type functionality
    this->onLoad(); 

    // flag as loaded
    m_isLoaded = true;
} // inline void mace::Component::load()


/**
 * @fn Component::unload()
 * @brief Unload this Component.
 * @author Joshua McLean
 * @date 2012/06/21 13:59
 *
 * Call Component::onUnload() and flag this Component as not loaded.
 */
void 
mace::Component::unload() {

    // don't unload if we're not loaded
    if( !isLoaded() ) return;

    boost::format formatter( "Unloading '%s'" );
    formatter % getName();
    mace::Log::message( formatter.str() );

    // overridden to provide per-Component-type functionality
    this->preUnload();

    // deregister from component map
    removeFromComponentMap( getName() );

    // no longer loaded
    m_isLoaded = false;

    // object suicide
    delete this;
} // inline void mace::Component::load() 


/**
 * @fn Component::update()
 * @brief Update this Component.
 * @param a_recurse True to recurse through Component tree, false to update only this Component.
 *   Defaults to true.
 * @author Joshua McLean
 * @date 6/19/2012 12:28 AM
 *
 * Tell the next Component to update and call Component::onUpdate().
 */
void 
mace::Component::update() {

    // don't update if we're not loaded and created
    if( !isLoaded() || !isCreated() ) return;

    // update this    
    this->onUpdate( m_updateTimer.getMillisecondsCPU() );

    m_updateTimer.reset();
} // void mace::Component::update()


//
// PUBLIC MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Component::getFullName() const
 * @brief Get the full name of the Component, qualified by its parent(s).
 * @author Joshua McLean
 * @date 2012/07/26 18:02
 */
const std::string
mace::Component::getFullName() const {
    if( m_pParentEntity == nullptr ) {
        return getName();
    } else {
        std::string name = getName();

        if( name == "" ) {
            name = "ANONYMOUS COMPONENT";
        }

        boost::format fmt( "%s :: %s" );
        fmt % m_pParentEntity->getName() % name;
        return fmt.str();
    }
} // const std::string mace::Component::getFullName() const


/**
 * @fn Component::getName() const
 * @brief Get the name (handle) of this Component.
 * @return A string containing the name (handle) of this Component.
 * @author Joshua McLean
 * @date 2012/06/21 11:31
 *
 * Return the name (handle) of this Component. Useful to check whether Component::doRegister()
 * changed the name of this Component to ensure uniqueness.
 */
inline
const std::string 
mace::Component::getName() const { 
    return m_name; 
} // inline const std::string mace::Component::getName() const


/**
 * @fn Component::getParentEntity() const
 * @brief Get the owning Entity for this Component.
 * @return A Component pointer to the owning Entity for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 11:32
 *
 * Return a Component pointer to the owning Entity for this Component. In order to make full use
 * of the returned data, must be cast into a pointer to Entity.
 */
//inline 
mace::Component * const
mace::Component::getParentEntity() const { 
    return m_pParentEntity; 
} // inline const mace::Component *mace::Component::getParentEntity() const


/**
 * @fn Component::isCreated() const
 * @brief Determine whether the component is created.
 * @return True if the component is created; false otherwise.
 * @author Joshua McLean
 * @date 2012/10/08 12:52
 *
 * Return the boolean value for whether the component is created.
 */
inline 
const bool 
mace::Component::isCreated() const {
    return m_isCreated;
}


/**
 * @fn Component::isLoaded() const
 * @brief Get whether this Component has been loaded.
 * @return A boolean specifying whether this Component has been loaded.
 * @author Joshua McLean
 * @date 2012/06/21 11:33
 *
 * Return whether this Component has been loaded, which is flagged in Component::onLoad().
 */
const bool 
mace::Component::isLoaded() const { 
    return m_isLoaded; 
} // const bool mace::Component::isLoaded() const


//
// PUBLIC MEMBER FUNCTIONS - MUTATORS
//

/**
 * @fn Component::setName( const std::string & )
 * @brief Set the name (handle) for this Component.
 * @param a_name The name (handle) for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 11:37
 *
 * If the Component is already registered, first remove the old entry for this Component from the
 * registry, then register this Component under the new name (handle). Otherwise, directly set
 * this Component's name (handle) to the given name (handle). NOTE: This does not guarantee that
 * a_name will be the final name (handle) of the Component if it has already been registered
 * because registration will ensure the name (handle) is unique. Remember to use getName() to get 
 * the name (handle) as it is finally registered.
 */
void 
mace::Component::setName( const std::string &a_rName ) { 
    auto prevName = m_name;

    m_name = a_rName;

    if( isLoaded() ) {
        doRegister( m_name );
    }

    // overridden to provide per-Component-type functionality
    onNameChanged( prevName );
}


/**
 * @fn Component::doRegister( const std::string & )
 * @brief Perform registration for the component.
 * @param a_rSuggestedName The suggested name for the component.
 * @author Joshua McLean
 * @date 2012/10/08 12:53
 *
 * Create a unique name for this component based on the suggested name.
 */
void
mace::Component::doRegister( const std::string &a_rSuggestedName ) {
    std::string originalName = getName();
    std::string originalFullName = getFullName();

    // if we're already loaded, we have to remove this Component's entry first
    if( isLoaded() ) {
        Component::removeFromComponentMap( getName() );
    }

    setName( strToUpper( Component::uniqueifyName( a_rSuggestedName ) ) );
    Component::addToComponentMap( m_name, this );

    boost::format fmt( "Registered '%s' ('%s') as '%s' ('%s')" );
    fmt % originalName % originalFullName % getName() % getFullName();
    Log::message( fmt.str() );
} // inline void mace::Component::setName( const std::string &a_name )


/**
 * @fn Component::setParentEntity( Component * const )
 * @brief Set an Entity as the parent of this Component.
 * @param a_pParentEntity A pointer to the parent Entity (as a Component pointer).
 * @author Joshua McLean
 * @date 6/19/2012 12:28 AM
 *
 * Change the parent of this Component to the given Entity, which must be given as a Component to
 * prevent circular dependency. If the new parent is nullptr and this Component is still registered,
 * first deregister this Component. Then set the new value for the parent Entity.
 */
void
mace::Component::setParentEntity( Component * const a_pParentEntity ) {
    boost::format formatter( "Setting parent of '%s' to '%s'" );
    formatter % getFullName() 
        % ( a_pParentEntity == nullptr ? "nullptr" : a_pParentEntity->getFullName() );
    mace::Log::message( formatter.str() );

   // if we're registered to a different parent, deregister from it
   if( a_pParentEntity == nullptr ) {
      a_pParentEntity->removeComponent( getName() );
   }

    m_pParentEntity = a_pParentEntity;
} // void mace::Component::setParentEntity( Component * const a_pParentEntity )


//
// PROTECTED STATIC MEMBER FUNCTIONS
//


/**
 * @fn Component::addToComponentMap( const std::string &, Component * const )
 * @brief Add a Component to the map of strings to components.
 * @param a_componentName The name (handle) to which the new Component will be registered.
 * @param a_pComponent A pointer to the Component which will be added to the registry.
 * @author Joshua McLean
 * @date 2012/06/20 15:48
 *
 * Check to ensure that the given name (handle) is not in use. If it is, append a numeral to
 * ensure the name (handle) is unique. Add an entry matching the name to the Component pointer to
 * the registry. Return the final name (handle) of the Component (in case it has changed).
 */
bool
// [static]
mace::Component::addToComponentMap(
    const std::string &a_rComponentName, 
    Component * const a_pComponent 
) {
    return Component::s_componentMap.add( a_rComponentName, a_pComponent );
} // std::string mace::Component::addToComponentList( const std::string &a_componentName, 
  //     Component *a_pComponent )


/**
 * @fn Component::removeFromComponentMap( const std::string & )
 * @brief Remove a component of the given name from the component map.
 * @param a_rComponentName The name of the component to be removed.
 * @author Joshua McLean
 * @date 2012/10/08 12:57
 *
 * Find the component who has the given name and remove it from the component map.
 */
bool
// [static]
mace::Component::removeFromComponentMap( const std::string &a_rComponentName ) {
    return s_componentMap.remove( a_rComponentName );
} // bool mace::Component::removeFromComponentMap( const std::string &a_rComponentName )


/**
 * @fn Component::eraseAllComponents()
 * @brief Erase all components in the component map.
 * @author Joshua McLean
 * @date 2012/10/08 13:14
 *
 * While there are components in the component map, remove, destroy, and unload the first entry.
 */
void
// [static]
mace::Component::eraseAllComponents() {
    while( !s_componentMap.isEmpty() ) {
        auto key = s_componentMap.getKeyAt( 0 );
        auto comp = s_componentMap.getValue( key );

        s_componentMap.remove( key );

        comp->destroy();
        comp->unload();
    }
}


/**
 * @fn Component::findComponent( const std::string & )
 * @brief Find a registered Component with the given name (handle).
 * @param a_componentName The name (handle) of the Component to be found.
 * @return A pointer to the Component if found; nullptr if not found.
 * @author Joshua McLean
 * @date 2012/06/20 15:39
 *
 * Search the Component registry for the given Component name (handle). Return a pointer to the
 * Component if found, else return nullptr.
 */
mace::Component *
// [static]
mace::Component::findComponent( const std::string &a_rComponentName ) { 
    return Component::s_componentMap.hasKey( a_rComponentName ) 
        ? Component::s_componentMap.getValue( a_rComponentName )
        : nullptr;
} // mace::Component *mace::Component::find( const std::string &a_componentName )


//
// PROTECTED MEMBER FUNCTIONS
//

/**
 * @fn Component::removeComponent( const std::string & )
 * @brief Remove a child component.
 * @param a_rComponentName The name of the component to be removed.
 * @author Joshua McLean
 * @date 2012/10/08 12:59
 *
 * Components cannot have components as children, so simply return false. This is only implemented
 * to provide a facility for entities to do the same, circumventing the lack of reflection in C++.
 */
bool
// [virtual]
mace::Component::removeComponent( const std::string &a_rComponentName ) {

    // Component can't have children so no remove
    return false;
} // bool mace::Component::removeComponent( const std::string &a_rComponentName )


/**
 * @fn Component::removeComponent( Component * const )
 * @brief Remove a child component.
 * @param a_pComponent A pointer to the component to remove.
 * @author Joshua McLean
 * @date 2012/10/08 12:59
 *
 * Components cannot have components as children, so simply return false. This is only implemented
 * to provide a facility for entities to do the same, circumventing the lack of reflection in C++.
 */
bool 
// [virtual]
mace::Component::removeComponent( Component * const a_pComponent ) {

    // Component can't have children so no remove
    return false;
} // bool mace::Component::removeComponent( Component * const a_pComponent ) 


//
// PROTECTED MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Component::getTypeName()
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string
mace::Component::getTypeName() {
    return "Component";
} // std::string mace::Component::getTypeName()


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn Component::onCreate()
 * @brief Post-creation functionality for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 14:11
 *
 * Called by Component::create(). Should create any child Components for this Component. Should NOT 
 * allocate memory within this Component - this should be handled by Component::onLoad().
 */

void 
// [virtual]
mace::Component::onCreate()
{} // void mace::Component::onCreate()


/**
 * @fn Component::onDestroy()
 * @brief Post-destruction functionality for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 14:13
 *
 * Called by Component::destroy(). Should remove any child Components created by
 * Component::create(). Should NOT delete allocated memory within this Component - this should be
 * handled by Component::onUnload().
 */

void 
// [virtual]
mace::Component::onDestroy() 
{} // void mace::Component::onDestroy()


/**
 * @fn Component::onLoad()
 * @brief Post-load functionality for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 14:09
 *
 * Called by Component::load(). Should allocate memory for data to be loaded for this Component.
 * Should NOT set up the initial state - this should be handled by Component::onCreate().
 */

void
// [virtual]
mace::Component::onLoad() 
{} // void mace::Component::onLoad() 


/**
 * @fn Component::onNameChanged()
 * @brief Post-name-changed functionality for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 14:25
 *
 * Called when the name of the Component is changed. Should be overridden to provide any internal
 * functionality necessary upon a name change, such as re-registering an entry in a map.
 */
void
// [virtual]
mace::Component::onNameChanged( const std::string &a_prevName)
{} // void mace::Component::onNameChanged( const std::string &a_prevName)


/**
 * @fn Component::onUpdate( const float a_dtms )
 * @param a_dtms The number of milliseconds that have passed since the last call to this function.
 * @brief Post-update functionality for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 14:20
 *
 * Called by Component::update(). This is the core function to implement Component functionality.
 * This function is called as quickly as possible to update the Component in real time.
 */

void
// [virtual]
mace::Component::onUpdate( const float a_dtms ) 
{} // void mace::Component::onUpdate( const float a_dtms ) 


/**
 * @fn Component::onUnload()
 * @brief Post-unload functionality for this Component.
 * @author Joshua McLean
 * @date 2012/06/21 14:18
 *
 * Called by Component::unload(). Should deallocate memory for data loaded for this Component,
 * undoing any allocations done in Component::load().
 */

void 
// [virtual]
mace::Component::preUnload() 
{} // void mace::Component::preUnload() 


//
// PROTECTED STATIC MEMBER VARIABLES
//

mace::StringMap< mace::Component * > mace::Component::s_componentMap;


//
// PROTECTED COPY CTOR
//

/**
 * @fn Component::Component( const mace::Component & )
 * @brief Copy constructor.
 * @param a_rComponent The Component to be cloned.
 * @author Joshua McLean
 * @date 2012/06/23 9:57
 *
 * Implicitly creates a copy of the given Component. Kept Protected to prevent use outside of
 * Component::clone() which is the preferred method for creating a cloned Component object, allowing
 * a derived class to implement its own clone()-called copy constructor.
 */
mace::Component::Component( const mace::Component &a_rComponent ) :
    m_isCreated( false ),
    m_isLoaded( false ),
    m_isMarkedForDestroy( false ),
    m_name( a_rComponent.getName() ),
    m_pParentEntity( nullptr )
{} // mace::Component::Component( const mace::Component &a_rComponent )


//
// PRIVATE STATIC MEMBER FUNCTIONS
//

/**
 * @fn Component::uniqueifyName( const std::string & )
 * @brief Create a unique name by appending a unique number to the end of the name.
 * @param a_rSuggestedName The suggested name to begin with.
 * @author Joshua McLean
 * @date 2012/10/08 13:11
 *
 * Check if the component map has the target name. If it does, append a number to the name, starting
 * with 2, then check again until we find a unique name. Return the unique name in all caps.
 */
std::string 
// [static]
mace::Component::uniqueifyName( const std::string &a_rSuggestedName ) {
    std::string uniqueName = a_rSuggestedName;

    int num = 1;
    while( Component::s_componentMap.hasKey( uniqueName ) ) {

        // start with the given name
        uniqueName = a_rSuggestedName;

        // append the next number
        ++num;
        char numStr[ 4 ];
        itoa( num, numStr, 10 );
        uniqueName.append( numStr );
    }

    // component names are all caps
    return strToUpper( uniqueName );
} // std::string mace::Component::uniqueifyName( const std::string &a_rSuggestedName )
