#include "mace/Vector3.h"


//
// Operator Functions
//


/**
 * @fn Vector3::operator+( const Vector3 & ) const
 * @brief Add two vectors together.
 * @param a_rVec The vector to add to this one.
 * @param A vector representing the sum of the two vectors.
 * @author Joshua McLean
 * @date 2012/10/08 13:29
 *
 * If both vectors are points, throw an exception. Otherwise, sum all the parts into a new vector
 * and return that vector.
 */
mace::Vector3 
mace::Vector3::operator+( const mace::Vector3 &a_rVec ) const {

    // point + point doesn't make sense
    if( m_isPoint && a_rVec.m_isPoint ) {
        throw std::exception( "Cannot add points together." );
    }
    
    Vector3 totalVec;
    totalVec.x = x + a_rVec.x;
    totalVec.y = y + a_rVec.y;
    totalVec.z = z + a_rVec.z;

    // if we succeeded in addition, the result is a vector
    totalVec.m_isPoint = false;

    return totalVec;
} // mace::Vector3 mace::Vector3::operator+( const mace::Vector3 &a_rVec ) const


/**
 * @fn Vector3::operator-() const
 * @brief Get the negative of this vector.
 * @return A vector containing the negative value of this vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:30
 *
 * Return a vector with negative parts.
 */
mace::Vector3 
mace::Vector3::operator-() const {
    return Vector3( -x, -y, -z );
} // mace::Vector3 mace::Vector3::operator-() const


/**
 * @fn Vector3::operator-( const Vector3 & ) const
 * @brief Subtract a vector from this one.
 * @param a_rVec The vector to subtract.
 * @return A vector containing the value of this one minus the given vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:31
 *
 * Return the negative of the given vector plus this vector.
 */
mace::Vector3 
mace::Vector3::operator-( const mace::Vector3 &a_rVec ) const {
    return -a_rVec + *this;
} // mace::Vector3 mace::Vector3::operator-( const mace::Vector3 &a_rVec ) const


/**
 * @fn Vector3::operator*( const float ) const
 * @brief Multiply this vector by a scalar value.
 * @param a_scale The scalar value.
 * @return A vector containing this vector scaled by the given value.
 * @author Joshua McLean
 * @date 2012/10/08 13:32
 *
 * Return a vector with this vector's parts multiplied by the scalar.
 */
mace::Vector3 
mace::Vector3::operator*( const float a_scalar ) const {
    return Vector3( x * a_scalar, y * a_scalar, z * a_scalar );
} // mace::Vector3 mace::Vector3::operator*( const float a_scalar ) const


/**
 * @fn Vector3::operator*=( const float )
 * @brief Multiply this vector by a scalar value and set it to the result.
 * @param a_scale The scalar value.
 * @return This vector after being multiplied by the scalar.
 * @author Joshua McLean
 * @date 2012/10/08 13:33
 *
 * Multiply this by the given scale, set this value to the given scale, then return this.
 */
mace::Vector3 
mace::Vector3::operator*=( const float scale ) {
    *this = *this * scale;

    return *this;
} // mace::Vector3 mace::Vector3::operator*=( const float scale )


/**
 * @fn Vector3::operator=( const Vector3 & )
 * @brief Set this vector equal to another vector.
 * @param a_rVec The vector whose values this vector will be set to.
 * @return This vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:34
 *
 * Set each component of this vector equal to the relevant component of the given vector, then
 * return this.
 */
mace::Vector3 
mace::Vector3::operator=( const mace::Vector3 &a_vec ) {
    x = a_vec.x;
    y = a_vec.y;
    z = a_vec.z;

    return *this;
} // mace::Vector3 mace::Vector3::operator=( const mace::Vector3 &a_vec )


//
// Other Public Functions
//

/**
 * @fn Vector3::const Vector3 & ) const
 * @brief Calculate the cross product of this vector with another vector.
 * @param The other vector.
 * @return A vector containing the cross product of this vector with the other vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:35
 *
 * If either vector is a point, throw an exception, as cross product can only be performed on a vector
 * with another vector. Calculate the cross product using the appropriate formula and return the 
 * result vector.
 */
mace::Vector3 
mace::Vector3::cross( const mace::Vector3 &a_vec ) const {
    if( m_isPoint || a_vec.m_isPoint ) {
        throw std::exception( "Cannot dot points or point with vector." );
    }

    Vector3 retVec;

	retVec.x = y * a_vec.z - a_vec.y * z;
	retVec.y = - ( x * a_vec.z - z * a_vec.x );
	retVec.z = x * a_vec.y - y * a_vec.x;

	return retVec;
} // mace::Vector3 mace::Vector3::cross( const mace::Vector3 &vec ) const


/**
 * @fn Vector3::dot( const Vector3 & ) const
 * @brief Calculate the dot product of this vector with another.
 * @param a_vec The other vector.
 * @return The resulting dot product.
 * @author Joshua McLean
 * @date 2012/10/08 13:36
 *
 * If either vector is a point, throw an exception, as dot product can only be performed on a vector
 * with another vector. Calculate the dot product using the appropriate formula and return the 
 * result.
 */
float 
mace::Vector3::dot( const mace::Vector3 &a_vec ) const {
    if( m_isPoint || a_vec.m_isPoint ) {
        throw std::exception( "Cannot dot points or point with vector." );
    }

    return x * a_vec.x + y * a_vec.y + z * a_vec.z;
} // float mace::Vector3::dot( const mace::Vector3 &vec ) const


/**
 * @fn Vector3::magnitude() const
 * @brief Calculate the magnitude of this vector.
 * @return A float containing the magnitude of this vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:37
 *
 * Calculate the magnitude of the vector and return it.
 */
float 
mace::Vector3::magnitude() const {
    return sqrt( x * x + y * y + z * z );
} // float mace::Vector3::magnitude() const


/**
 * @fn Vector3::normalize()
 * @brief Normalize this vector.
 * @author Joshua McLean
 * @date 2012/10/08 13:38
 *
 * Get the magnitude of the vector and divide each component by it, creating the normalized vector.
 */
void 
mace::Vector3::normalize() {
    float mag = magnitude();
    x /= mag;
    y /= mag;
    z /= mag;
} // void mace::Vector3::normalize()


/**
 * @fn Vector3::toString( const uint32_t ) const
 * @brief Get a string containing information about this vector.
 * @param The desired precision for values in the string.
 * @return A string containing the value of the vector to the given precision.
 * @author Joshua McLean
 * @date 2012/10/08 13:40
 *
 * Construct a formatted string to match the formatting required of the given precision, then fill
 * in that constructed string with the values from the vector. Return this final string.
 */
std::string mace::Vector3::toString( const uint32_t a_precision ) const {
    boost::format formattingFmt( "{%%.%df, %%.%df, %%.%df}" );
    formattingFmt % a_precision % a_precision % a_precision;

    boost::format fmt( formattingFmt.str() );
    fmt % x % y % z;
    return fmt.str();
}


//
// Constants
//

const mace::Vector3 mace::Vector3::ZERO = mace::Vector3();
const mace::Vector3 mace::Vector3::UNIT = mace::Vector3( 1.0f );
const mace::Vector3 mace::Vector3::UNIT_X = mace::Vector3( 1.0f, 0.0f, 0.0f );
const mace::Vector3 mace::Vector3::UNIT_Y = mace::Vector3( 0.0f, 1.0f, 0.0f );
const mace::Vector3 mace::Vector3::UNIT_Z = mace::Vector3( 0.0f, 0.0f, 1.0f );
