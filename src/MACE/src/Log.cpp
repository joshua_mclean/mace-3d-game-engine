#include "mace/Log.h"


/**
 * @fn Log::close()
 * @brief Close the log file.
 * @author Joshua McLean
 * @date 2012/10/08 14:43
 *
 * If the log file exists, close it. This MUST be called prior to program termination after the log
 * is initialized if a file has been written to.
 */
void 
// [static]
mace::Log::close() { 
    std::fclose( m_file );
} // void mace::Log::close() 


/**
 * @fn Log::initialize( const std::string & )
 * @brief Initialize the log with the given filename.
 * @param a_rFilename The name of the file to which this log will write.
 * @author Joshua McLean
 * @date 2012/10/08 14:40
 *
 * If the filename is blank, throw an exception. Otherwise, create the file for output, writing the 
 * time at which the log is initialized. The file is kept open for the duration of the log's life 
 * and MUST be closed before terminating the program.
 */
void
// [static]
mace::Log::initialize( const std::string &a_rFilename = "" ) {
    if( a_rFilename == "" ) {
        throw new Exception( Exception::ILLEGAL_OPERATION, "Filename cannot be blank.", 
            "Log::initialize()" );
    }

    m_file = std::fopen( a_rFilename.c_str(), "w" );

    boost::format fmt( "Log initialized (%d).\n\n" );
    fmt % time( nullptr );
    std::fprintf( m_file, fmt.str().c_str() );

    std::fclose( m_file );

    m_file = std::fopen( a_rFilename.c_str(), "a+" );
} // void mace::Log::initialize( const std::string &a_rFilename = "" )


/**
 * @fn Log::message( const std::string & )
 * @brief Print a message to the log.
 * @param The message to be printed.
 * @author Joshua McLean
 * @date 2012/10/08 14:47
 *
 * If the log file isn't open, return. Otherwise, append a newline to the message and print it to
 * the file. If we wish to print to standard output, also print to standard output.
 */
void 
// [static]
mace::Log::message( const std::string &a_rMessage ) {

    // if the file isn't open, don't try to log
    if( m_file == nullptr ) return;

    auto msg = a_rMessage;
    msg += "\n";

    // print to file
    std::fprintf( m_file, msg.c_str() );

    if( m_printToOutput ) {
        std::cout << msg;
    }
} // void mace::Log::message( const std::string &a_rMessage )


/**
 * @fn Log::printToOutput( const bool )
 * @brief Set whether to print to output.
 * @param a_val Whether to print to output.
 * @author Joshua McLean
 * @date 2012/10/08 14:48
 *
 * Set the value of whether to print to output.
 */
void 
// [static]
mace::Log::printToOutput( const bool a_val ) {
    m_printToOutput = a_val;
} // void mace::Log::printToOutput( const bool a_val )


FILE *
// [static]
mace::Log::m_file = nullptr;


bool 
// [static]
mace::Log::m_printToOutput = false;
