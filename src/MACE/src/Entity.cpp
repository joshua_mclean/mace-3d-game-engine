#include "mace/Entity.h"


//
// PUBLIC CTOR / DTOR
//

/**
 * @fn Entity::Entity( const std::string & )
 * @brief Constructor.
 * @param a_name The name (handle) for the Entity.
 * @author Joshua McLean
 * @date 2012/06/21 17:20
 *
 * Create the Entity with the given name (handle) and initialize child pointers to nullptr.
 */
mace::Entity::Entity( 
    const std::string &a_rName // = ""
) : 
    Component( a_rName )
{} // mace::Entity::Entity( const std::string &a_rName )


/**
 * @fn Entity::~Entity()
 * @brief Destructor.
 * @author Joshua McLean
 * @date 2012/06/21 17:31
 *
 * When the Entity is deleted, all Component objects attached to it are also deleted. Thus, if any
 * Component is to be reused, it must be removed before the owning Entity is deleted.
 */
mace::Entity::~Entity() {
} // mace::Entity::~Entity()


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Entity::addComponent( Component * const )
 * @brief Add a Component as a child to this Entity.
 * @param a_pComponent A pointer to the Component which will be added to the Entity.
 * @return True if the Component was added successfully, false if there was a problem adding it.
 * @author Joshua McLean
 * @date 2012/06/23 9:44
 *
 * If the given Component is this Entity, log a warning and return false. If the given Component
 * pointer is nullptr, return immediately. Add the Component as the last child of this Entity 
 * (which is also the first, if there are no children). Set the parent of the given Component to 
 * this Entity. If this Entity has already been created, create the newly added component. If this 
 * Entity has already been registered, register the newly added Component. Return true (success).
 */
bool 
mace::Entity::addComponent( mace::Component * const a_pComponent ) {
    
    // don't allow adding a component (entity) to itself
    if( a_pComponent == this ) {
        boost::format format( "WARNING: Attempt to add entity %s to itself ignored." );
        format % getFullName();
        Log::message( format.str() );

        return false;
    }

    // can't add null
    if( a_pComponent == nullptr ) return false;

    boost::format formatter( "Adding '%s' as child of '%s'" );
    formatter % a_pComponent->getFullName().c_str() % getName().c_str();
    mace::Log::message( formatter.str() );

    // can't add component that is already added
    if( hasComponent( a_pComponent ) ) return false;

    // add the component
    m_children.push_back( a_pComponent );

    // tell child that this is the parent
    a_pComponent->setParentEntity( this );

    // successful add
    return true;
} // bool mace::Entity::addComponent( mace::Component * const a_pComponent )


/**
 * @fn Entity::clone( const std::string & )
 * @brief Clone this.
 * @param a_rCloneName The name for the new clone.
 * @return A pointer to the newly created clone.
 * @author Joshua McLean
 * @date 2012/10/08 12:24
 *
 * Construct a deep clone of the entity, including clones of all of its child components.
*/
mace::Entity * const
mace::Entity::clone( 
    const std::string &a_rCloneName // = ""
) const {
    auto pEntity = new Entity( *this );

    if( a_rCloneName != "" ) {
        pEntity->setName( a_rCloneName );
    }

    boost::format fmt( "Created clone of %s called %s" );
    fmt % getFullName() % pEntity->getFullName();
    mace::Log::message( fmt.str() );

    return pEntity;
}


/**
 * @fn Entity::hasComponent( Component * const )
 * @brief Determine whether this Entity has the given Component as a child.
 * @param a_pComponent The Component to check.
 * @return True if the Component is a child of this Entity, false otherwise.
 * @author Joshua McLean
 * @date 2012/06/23 12:31
 *
 * Starting with the first child Component, iterate through all children Component objects until the
 * target is found, at which point return true. If it is not found, return false.
 */
bool 
mace::Entity::hasComponent( mace::Component * const a_pTargetComponent ) {
    for( auto iter = m_children.begin(); iter != m_children.end(); ++iter ) {
        auto pCurComp = *iter;

        // if target component found
        if( pCurComp == a_pTargetComponent ) return true;
    }

    // target component was not found
    return false;
} // bool mace::Entity::hasComponent( mace::Component * const a_pComponent )


/**
 * @fn Entity::removeComponent( Component * const )
 * @brief Remove the given Component from this Entity.
 * @return True if the Component was removed successfully, false if there was a problem.
 * @author Joshua McLean
 * @date 2012/06/23 12:32
 *
 * If the Component does not exist in this Entity, return false. Remove the Component from its
 * containing list (which is the list of Components for this Entity) and return true.
 */
bool 
mace::Entity::removeComponent( mace::Component * const a_pTargetComponent ) {
    for( auto iter = m_children.begin(); iter != m_children.end(); ++iter ) {
        auto pCurComp = *iter;

        // if target component found
        if( pCurComp == a_pTargetComponent ) {

            // remove the component
            m_children.erase( iter );

            // success
            return true;
        }
    }

    // target component was not found
    return false;
} // bool mace::Entity::removeComponent( mace::Component * const a_pComponent )


/**
 * @fn Entity::removeComponent( const std::string & )
 * @brief Remove a Component by name (handle).
 * @return True if the Component was removed successfully, false if there was a problem.
 * @author Joshua McLean
 * @date 2012/06/23 12:30
 *
 * Find a pointer to the Component. If the name (handle) matches no Component, return true.
 * Otherwise, attempt to remove the Component and return the respective result.
 */
bool 
mace::Entity::removeComponent( 
    const std::string &a_rTargetName 
) {
    for( auto iter = m_children.begin(); iter != m_children.end(); ++iter ) {
        auto pCurComp = *iter;

        // if target component found, remove it and return success
        if( pCurComp->getName() == a_rTargetName ) {
            m_children.erase( iter );
            return true;
        }
    }

    // target component was not found
    return false;
} // bool mace::Entity::removeComponent( const std::string &a_rComponentName )


//
// PROTECTED MEMBER FUNCTIONS
//

/**
 * @fn Entity::Entity( const Entity & )
 * @brief Copy constructor.
 * @author Joshua McLean
 * @date 2012/06/21 17:25
 *
 * Create a copy of the given Entity. Protected to prevent use outside of Entity::clone() which is 
 * the preferred method for creating a cloned Component object as it is possible to override it.
 */
mace::Entity::Entity( const mace::Entity &a_rEntity ) :
    Component( a_rEntity.getName() )
{

    // clone all child components from the source entity
    for( auto iter = a_rEntity.m_children.cbegin(); iter != a_rEntity.m_children.cend(); ++iter ) {
        auto pCurComp = *iter;

        addComponent( pCurComp->clone() );
    }
} // mace::Entity::Entity( const mace::Entity &a_rEntity )


/**
 * @fn Entity::onCreate()
 * @brief Called after this Entity has been created.
 * @author Joshua McLean
 * @date 2012/06/23 12:06
 *
 * Recursively create all child Components.
 */
void 
mace::Entity::onCreate() {

    // create children recursively
    for( auto iter = m_children.cbegin(); iter != m_children.cend(); ++iter ) {
        auto pComp = *iter;

        if( !pComp->isLoaded() ) {
            boost::format fmt( "WARNING: Attempted to create %s without loading first." );
            fmt % pComp->getFullName();
            Log::message( fmt.str() );

            continue;
        }

        pComp->create();
    }
} // void mace::Entity::onCreate()


/**
 * @fn Entity::onDestroy()
 * @brief Called after this Entity has been destroyed.
 * @author Joshua McLean
 * @date 2012/06/23 12:08
 *
 * Recursively destroy all child Components.
 */
void
// [override]
mace::Entity::onDestroy() {

    // destroy children recursively
    for( auto iter = m_children.cbegin(); iter != m_children.cend(); ++iter ) {
        (*iter)->destroy();
    }
} // void mace::Entity::onDestroy()


/**
 * @fn Entity::onLoad()
 * @brief Called after this entity is loaded.
 * @author Joshua McLean
 * @date 2012/10/08 23:57
 *
 * Recursively load all children.
 */
void
// [override]
mace::Entity::onLoad() {
    for( auto iter = m_children.cbegin(); iter != m_children.cend(); ++iter ) {
        (*iter)->load();
    }
} // void mace::Entity::onLoad()


/**
 * @fn Entity::onUpdate( const float )
 * @brief Called after this Entity has been updated.
 * @param a_dtms The number of milliseconds that have passed between the previous two cycles.
 * @author Joshua McLean
 * @date 2012/06/23 12:08
 *
 * Recursively update all child Components.
 */
void
// [override]
mace::Entity::onUpdate( const float a_dtms ) {

    // update children recursively
    for( auto iter = m_children.cbegin(); iter != m_children.cend(); ++iter ) {
        auto curComp = *iter;

        curComp->update();

        if( curComp->isMarkedForDestroy() ) {
            curComp->destroy();
        }
    }
} // void mace::Entity::onUpdate( const float a_dtms )


/**
 * @fn Entity::preUnload()
 * @brief Called before this function is to be unloaded.
 * @author Joshua McLean
 * @date 2012/10/08 17:27
 *
 * When the entity is unloaded, unload all of its children.
 */
void
// [override]
mace::Entity::preUnload() {

    // unload children recursively
    while( m_children.size() > 0 ) {
        auto iter = m_children.begin();
        auto pComp = *iter;

        pComp->unload();
    }

    return;
} // void mace::Entity::preUnload()
