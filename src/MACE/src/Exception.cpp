#include "mace/Exception.h"


//
// PUBLIC CTOR
//

/**
 * @fn Exception::Exception( const category_t, const std::string &, const std::string & )
 * @brief Constructor.
 * @param a_category The category for the exception.
 * @param a_rMessage The message for the exception.
 * @param a_rSource The name of the source of the exception, typically the containing function name.
 * @author Joshua McLean
 * @date 2012/10/08 15:27
 *
 * Construct the exception by combining all of the given information into a full information string.
 */
mace::Exception::Exception( 
    const category_t a_category, 
    const std::string &a_rMessage, 
    const std::string &a_rSource 
) {
    m_fullMessage = "[";
    m_fullMessage += getCategoryMessage( a_category );
    m_fullMessage += "] ";
    m_fullMessage += a_rMessage;
    m_fullMessage += " [in ";
    m_fullMessage += a_rSource;
    m_fullMessage += "]";
}


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Exception::what() const
 * @brief Get the string describing the exception.
 * @return A C-style string containing the exception information.
 * @author Joshua McLean
 * @date 2012/10/08 15:29
 *
 * Return the full message converted to a C-style string.
 */
const char *
// [override]
mace::Exception::what() const { 
    return m_fullMessage.c_str();
}


//
// PROTECTED MEMBER FUNCTIONS
//

/**
 * @fn Exception::getCategoryMessage( const category_t )
 * @brief Get the message for a given category.
 * @param a_category The category whose message we want.
 * @return A string representing the given category, or "Unknown Error" if the category is invalid.
 * @author Joshua McLean
 * @date 2012/10/08 15:29
 *
 * Return the relevant string for the given category.
 */
std::string
mace::Exception::getCategoryMessage( const category_t a_category ) {
    switch( a_category ) {
        case ILLEGAL_OPERATION: return "Illegal Operation";
        case INITIALIZATION_ERROR: return "Initialization";
        case KEY_NOT_FOUND: return "Key Not Found";
        default: return "Unknown Error";
    }
}
