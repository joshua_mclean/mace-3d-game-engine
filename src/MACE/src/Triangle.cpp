#include "mace/Triangle.h"


//
// PUBLIC CTOR
//

/**
 * @fn Triangle::Triangle( const uint32_t, const uint32_t, const uint32_t )
 * @brief Constructor.
 * @author Joshua McLean
 * @param a_index1 The first index.
 * @param a_index2 The second index.
 * @param a_index3 The third index.
 * @date 2012/10/08 13:55
 *
 * Construct a triangle with the given indices.
 */
mace::Triangle::Triangle(
    const uint32_t a_index1, 
    const uint32_t a_index2, 
    const uint32_t a_index3 
) {
    index[ 0 ] = a_index1;
    index[ 1 ] = a_index2;
    index[ 2 ] = a_index3;
} // mace::Triangle::Triangle( const uint32_t a_index1, const uint32_t a_index2, 
    // const uint32_t a_index3 )
