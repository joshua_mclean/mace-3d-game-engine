#include "mace/Input.h"


//
// PUBLIC CTOR
//

/**
 * @fn Input::Input( const std::string & )
 * @brief Construct an input system with the given name.
 * @param a_rName The name for the input system.
 * @author Joshua McLean
 * @date 2012/10/08 21:53
 *
 * Construct an input system with the given name.
 */
mace::Input::Input(
    const std::string &a_rName // = ""
) : 
    Component( a_rName ), 
    m_pInputManager( nullptr ), 
    m_pKeyboard( nullptr )
{} // mace::Input::Input( const std::string &a_rName ) 


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn Input::addCommand( const CommandFunc &, const std::string &, const InputType, 
 *   const OIS::KeyCode )
 * @brief Add a command function using given input and name.
 * @param a_rCommandFunc The function to map to the input.
 * @param a_rCommandName The name (handle) for the command.
 * @param a_inputType The type of input (HELD, PRESSED, or RELEASED).
 * @author Joshua McLean
 * @date 2012/10/08 22:02
 *
 * Convert the command name to all caps. Then, add it to the relevant key map. If a default key is
 * given, assign it to the given command.
 */
void 
mace::Input::addCommand(
    const CommandFunc &a_rCommandFunc, 
    const std::string &a_rCommandName, 
    const InputType a_inputType, 
    const OIS::KeyCode a_defaultKey // = OIS::KC_UNASSIGNED 
) {
    // map the command name
    auto commandNameUpper = strToUpper( a_rCommandName );

    if( a_inputType == HELD ) {

        if( a_defaultKey != OIS::KC_UNASSIGNED ) {
            m_stringKeyHeldMap.insert( 
                StringKeyMap::value_type( commandNameUpper, a_defaultKey ) 
            );
        }

        m_stringCommandHeldMap.insert( 
            StringCommandMap::value_type( commandNameUpper, a_rCommandFunc ) 
        );
    } else if( a_inputType == PRESSED ) {
        if( a_defaultKey != OIS::KC_UNASSIGNED ) {
            m_stringKeyPressedMap.insert( 
                StringKeyMap::value_type( commandNameUpper, a_defaultKey ) 
            );
        }

        m_stringCommandPressedMap.insert(
            StringCommandMap::value_type( commandNameUpper, a_rCommandFunc )
            );
    } else if( a_inputType == RELEASED ) {
        if( a_defaultKey != OIS::KC_UNASSIGNED ) {
            m_stringKeyReleasedMap.insert( 
                StringKeyMap::value_type( commandNameUpper, a_defaultKey )
            );
        }

        m_stringCommandReleasedMap.insert( 
            StringCommandMap::value_type( commandNameUpper, a_rCommandFunc ) 
        );
    }

    // assign the default key if it is given
    if( a_defaultKey != OIS::KC_UNASSIGNED ) {
        bindKey( a_defaultKey, a_rCommandName, a_inputType );
    }
} // void mace::Input::addCommand( const CommandFunc &a_rCommandFunc, 
    // const std::string &a_rCommandName, const InputType a_inputType, 
    // const OIS::KeyCode a_defaultKey )


/**
 * @fn Input::bindKey( const OIS::KeyCode, const std::string &, InputType )
 * @brief Bind the given key to the given command of the given input type.
 * @param a_key The OIS key code for the desired key.
 * @param a_rCommandName The name (handle) for the command to bind.
 * @param a_inputType The type of input (HELD, PRESSED, or RELEASED).
 * @author Joshua McLean
 * @date 2012/10/08 22:30
 *
 * Get the command function with the given name and input type. If not found, return. Otherwise,
 * make sure the given command is not bound under the given input. Then, bind the key to the new
 * key by adding the pair of key and command to the map matching the given input type.
 */
void 
mace::Input::bindKey( 
    const OIS::KeyCode a_key, 
    const std::string &a_rCommandName, 
    InputType a_inputType 
) {
    auto commandFunc = getCommandFunc( a_rCommandName, a_inputType );
    if( commandFunc == nullptr ) return;

    unbindCommand( a_rCommandName, a_inputType );

    // add the new key binding
    if( a_inputType == HELD ) {
        m_keyHeldMap.insert( KeyCommandMap::value_type( a_key, commandFunc ) );
    } else if( a_inputType == PRESSED ) {
        m_keyPressedMap.insert( KeyCommandMap::value_type( a_key, commandFunc ) );
    } else if( a_inputType == RELEASED ) {
        m_keyReleasedMap.insert( KeyCommandMap::value_type( a_key, commandFunc ) );
    }
} // void mace::Input::bindKey( const OIS::KeyCode a_key, const std::string &a_rCommandName, 
    // InputType a_inputType )


/**
 * @fn Input::unbindCommand( const std::string &, const InputType )
 * @brief Unbind the given command on the given input type.
 * @param a_rCommandName The name of the command to unbind.
 * @param a_inputType The type of input (HELD, PRESSED, or RELEASED).
 * @author Joshua McLean
 * @date 2012/10/08 22:38
 *
 * Get the key for the given command and unbind it under the given input type.
 */
void 
mace::Input::unbindCommand( const std::string &a_rCommandName, const InputType a_inputType ) {
    unbindKey( getKey( a_rCommandName ), a_inputType );
} // void mace::Input::unbindCommand( const std::string &a_rCommandName,
    // const InputType a_inputType )


/**
 * @fn Input::unbindKey( const OIS::KeyCode, const InputType )
 * @brief Unbind the given key under the given input type.
 * @param a_key The OIS key code for the key to be unbound.
 * @param a_inputType The input type under which the key is to be unbound.
 * @author Joshua McLean
 * @date 2012/10/08 22:40
 *
 * Find the current binding for the given key under the given input type, then remove it from the
 * relevant key map.
 */
void 
mace::Input::unbindKey( const OIS::KeyCode a_key, const InputType a_inputType ) {
    if( a_inputType == HELD ) {
        auto currentBind = m_keyHeldMap.find( a_key );

        if( currentBind != m_keyHeldMap.end() ) {
            m_keyHeldMap.erase( currentBind );

            return;
        }
    } else if( a_inputType == PRESSED ) {
        auto currentBind = m_keyPressedMap.find( a_key );

        if( currentBind != m_keyPressedMap.end() ) {
            m_keyHeldMap.erase( currentBind );

            return;
        }
    } else if( a_inputType == RELEASED ) {
        auto currentBind = m_keyReleasedMap.find( a_key );

        if( currentBind != m_keyReleasedMap.end() ) {
            m_keyHeldMap.erase( currentBind );
        }
    }
} // void mace::Input::unbindKey( const OIS::KeyCode a_key, const InputType a_inputType )


//
// PUBLIC MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Input::isKeyDown( OIS::KeyCode )
 * @brief Determine whether the given key is down.
 * @return True if the key is down; false if it is up.
 * @author Joshua McLean
 * @date 2012/10/08 22:41
 *
 * Query the keyboard object for whether the given key is down, then return the result.
 */
bool
mace::Input::isKeyDown( OIS::KeyCode a_key ) {
    return m_pKeyboard->isKeyDown( a_key );
} // bool mace::Input::isKeyDown( OIS::KeyCode a_key )


/**
 * @fn Input::isKeyUp( OIS::KeyCode )
 * @brief Determine whether the given key is up.
 * @return True if the key is up; false if it is up.
 * @author Joshua McLean
 * @date 2012/10/08 22:42
 *
 * Essentially, an inverted alias for isKeyDown() - return the opposite of isKeyDown().
 */
bool
mace::Input::isKeyUp( OIS::KeyCode a_key ) {
    return !isKeyDown( a_key );
} // bool mace::Input::isKeyUp( OIS::KeyCode a_key )


/**
 * @fn Input::getKey( const std::string & )
 * @brief Get the key code mapped to the given command.
 * @param a_rCommandName The name of the command to look up.
 * @return The OIS key code mapped to the given command, or OIS::KC_UNASSIGNED if not assigned.
 * @author Joshua McLean
 * @date 2012/10/08 22:44
 *
 * Convert the given command name to all caps. Then, check whether the command is in the held map.
 * If not, return OIS::KC_UNASSIGNED. Otherwise, return the matching OIS key code.
 */
const OIS::KeyCode &
mace::Input::getKey( const std::string &a_rCommandName ) {
    std::string commandNameUpper = strToUpper( a_rCommandName );

    auto command = m_stringKeyHeldMap.find( commandNameUpper );
    if( command == m_stringKeyHeldMap.end() ) return OIS::KC_UNASSIGNED;

    return m_stringKeyHeldMap[ a_rCommandName ];
} // const OIS::KeyCode &mace::Input::getKey( const std::string &a_rCommandName )


/**
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string 
// [override]
mace::Input::getTypeName() {
    return "Input";
} // std::string mace::Input::getTypeName() 


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn Input::onCreate()
 * @brief Additional creation functionality.
 * @author Joshua McLean
 * @date 2012/10/08 22:45
 *
 * Get the OGRE window from the Graphics manager, then create the input system. Construct a keyboard
 * object to query for keyboard input and set the callback object to this.
 */
void 
mace::Input::onCreate() {
    OIS::ParamList paramList;
    size_t windowHnd = 0;
    std::ostringstream windowHndStr;

    // get window handle
    auto pGraphics = Component::findComponentOfType< Graphics >();
    pGraphics->getWindow()->getCustomAttribute( "WINDOW", &windowHnd );

    // fill parameter list
    windowHndStr << (unsigned int) windowHnd;
    paramList.insert( std::make_pair( std::string( "WINDOW" ), windowHndStr.str() ) );

    // create input system
    m_pInputManager = OIS::InputManager::createInputSystem( paramList );        

    // create the keyboard
    if( m_pInputManager->getNumberOfDevices( OIS::OISKeyboard ) > 0 ) {
        m_pKeyboard = static_cast< OIS::Keyboard * >( 
            m_pInputManager->createInputObject( OIS::OISKeyboard, true ) 
            );
        m_pKeyboard->setEventCallback( this );
    }
} // void mace::Input::onCreate()


/**
 * @fn Input::keyPressed( const OIS::KeyEvent )
 * @brief React to a key press.
 * @param a_rKeyEvent The OIS-generated key event data structure.
 * @return True if a key has been pressed; false otherwise.
 * @author Joshua McLean
 * @date 2012/10/08 22:48
 *
 * Get the key that has been pressed and check if it is in the pressed map. If it is not, return
 * false. Otherwise, execute the bound function and return true.
 */
bool
mace::Input::keyPressed( const OIS::KeyEvent &a_rKeyEvent ) {
    auto key = a_rKeyEvent.key;

    if( m_keyPressedMap.find( key ) == m_keyPressedMap.end() ) {

        // not a legal keypress
        return false;
    }

    // execute the bound functions
    m_keyPressedMap[ key ]();

    return true;
} // bool mace::Input::keyPressed( const OIS::KeyEvent &a_rKeyEvent )


/**
 * @fn Input::keyReleased( const OIS::KeyEvent )
 * @brief React to a key press.
 * @param a_rKeyEvent The OIS-generated key event data structure.
 * @return True if a key has been released; false otherwise.
 * @author Joshua McLean
 * @date 2012/10/08 22:48
 *
 * Get the key that has been released and check if it is in the released map. If it is not, return
 * false. Otherwise, execute the bound function and return true.
 */
bool 
mace::Input::keyReleased( const OIS::KeyEvent &a_rKeyEvent ) {
    auto key = a_rKeyEvent.key;

    if( m_keyReleasedMap.find( key ) == m_keyReleasedMap.end() ) {
        return false;
    }

    // execute the bound functions
    m_keyReleasedMap[ key ]();

    return true;
} // bool mace::Input::keyReleased( const OIS::KeyEvent &a_rKeyEvent )


/**
 * @fn Input::onUpdate( const float )
 * @brief Additional update functionality.
 * @param a_dtms The number of milliseconds that have passed between the previous update cycle and 
 *   this update cycle.
 * @author Joshua McLean
 * @date 2012/10/08 22:49
 *
 * Capture any input from the keyboard, the check for held keys. If a key being held is mapped to
 * a HELD function, execute the bound function.
 */
void
mace::Input::onUpdate( const float a_dtms ) {

    // update the keyboard state
    if( m_pKeyboard != nullptr ) {
        m_pKeyboard->capture();
    }

    // check for keys being held
    for( 
        KeyCommandMap::iterator iter = m_keyHeldMap.begin(); 
        iter != m_keyHeldMap.end();
        iter++
    ) {
            if( isKeyDown( iter->first ) ) {
                iter->second();
            }
    }
} // void mace::Input::onUpdate( const float a_dtms )


//
// PRIVATE MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn Input::getCommandFunc( const std::string &, const InputType )
 * @brief Get the command function bound to the given command name under the given input type.
 * @param a_rCommandName The name (handle) for the command.
 * @param a_inputType The type of input.
 * @return The command function matching the given parameters, or nullptr if no such command exists.
 * @author Joshua McLean
 * @date 2012/10/08 22:51
 *
 * Look up the given command name in the key map for the given input type. If it is not found,
 * return nullptr. Otherwise, return the bound command.
 */
mace::Input::CommandFunc
mace::Input::getCommandFunc( const std::string &a_rCommandName, const InputType a_inputType ) {
    std::string commandNameUpper = strToUpper( a_rCommandName );

    if( a_inputType == HELD ) {
        auto command = m_stringCommandHeldMap.find( commandNameUpper );
        if( command == m_stringCommandHeldMap.end() ) return nullptr;

        return m_stringCommandHeldMap[ commandNameUpper ];
    } else if( a_inputType == PRESSED ) {
        auto command = m_stringCommandPressedMap.find( commandNameUpper );
        if( command == m_stringCommandPressedMap.end() ) return nullptr;

        return m_stringCommandPressedMap[ commandNameUpper ];
    } else if( a_inputType == RELEASED ) {
        auto command = m_stringCommandReleasedMap.find( commandNameUpper );
        if( command == m_stringCommandReleasedMap.end() ) return nullptr;

        return m_stringCommandReleasedMap[ commandNameUpper ];
    }
} // mace::Input::CommandFunc mace::Input::getCommandFunc( const std::string &a_rCommandName, const InputType a_inputType )
