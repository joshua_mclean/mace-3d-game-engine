#include "mace/MoveXComponent.h"


//
// PUBLIC CTOR
//

/**
 * @fn MoveXComponent::MoveXComponent
 * @brief Default constructor.
 * @param a_rName The name for the Move X Component. Default is an empty string.
 * @author Joshua McLean
 * @date 2012/10/06 16:51
 *
 * Constructs a Move X Component with the given name, or an empty name if none is specified.
 */
mace::MoveXComponent::MoveXComponent( 
    const std::string &a_rName // = ""
) :
    MoveComponent( a_rName )
{} // mace::MoveXComponent::MoveXComponent( const std::string &a_name )


//
// PUBLIC MEMBER FUNCTIONS
//

/**
 * @fn MoveXComponent::clone( const std::string & )
 * @brief Create a clone of this Move X Component.
 * @param a_rCloneName The name for the cloned Move X Component.
 * @return A pointer to the new clone.
 * @author Joshua McLean
 * @date 2012/10/06 16:52
 *
 * Creates a deep clone of this Move X Component.
 */
mace::MoveXComponent * const 
mace::MoveXComponent::clone( 
    const std::string &a_rCloneName // = ""
) const {
    auto pMoveXComp = new MoveXComponent( *this );

    if( a_rCloneName != "" ) {
        pMoveXComp->setName( a_rCloneName );
    }

    return pMoveXComp;
} // mace::MoveXComponent * const mace::MoveXComponent::clone( const std::string &a_rCloneName ) 
    // const


//
// PUBLIC MEMBER FUNCTIONS - ACTIONS
//


/**
 * @fn MoveXComponent::moveEnd()
 * @brief Ends the move for this Move X Component.
 * @author Joshua McLean
 * @date 2012/10/06 16:52
 *
 * Ensures that the target physics exists and then tells it to nullify its velocity along the X 
 * axis.
 */
void
// [override]
mace::MoveXComponent::moveEnd() {
    checkPhysicsExists();
    m_pPhysicsComponent->setVelocityX( 0.0f );
} // void mace::MoveXComponent::moveEnd()


/**
 * @fn MoveXComponent::moveNegativeHeld()
 * @brief Called when the negative movement button is held.
 * @author Joshua McLean
 * @date 2012/10/06 16:53
 *
 * Ensures that the target physics exists and then tells it to move negatively along the X axis.
 */
void 
// [override]
mace::MoveXComponent::moveNegativeHeld() {
    checkPhysicsExists();
    m_pPhysicsComponent->setVelocityX( -MOVE_SPEED );
} // void mace::MoveXComponent::moveNegativeHeld()


/**
 * @fn MoveXComponent::movePositiveHeld()
 * @brief Called when the positive movement button is held.
 * @author Joshua McLean
 * @date 2012/10/06 16:53
 *
 * Ensures that the target physics exists and then tells it to move positively along the X axis.
 */
void 
// [override]
mace::MoveXComponent::movePositiveHeld() {
    checkPhysicsExists();
    m_pPhysicsComponent->setVelocityX( MOVE_SPEED );
} // void mace::MoveXComponent::movePositiveHeld()


//
// PROTECTED COPY CTOR
//

/**
 * @fn MoveXComponent::MoveXComponent( const MoveXComponent )
 * @brief Copy constructor.
 * @param a_rMoveXComp The Move X Component to be copied.
 * @author Joshua McLean
 * @date 2012/10/06 16:54
 *
 * Private copy constructor which is only callable through the deep clone function.
 */
mace::MoveXComponent::MoveXComponent( const mace::MoveXComponent &a_rMoveXComp ) :
    MoveComponent( a_rMoveXComp.getName() )
{} // mace::MoveXComponent::MoveXComponent( const mace::MoveXComponent &a_rMoveXComp )


//
// PROTECTED MEMBER FUNCTIONS - ACCESSORS
//

/**
 * @fn MoveXComponent::getTypeName()
 * @brief Get the name of this type.
 * @return A string containing the proper name of this type.
 * @author Joshua McLean
 * @date 2012/10/08 12:00
 *
 * Return the name of this type, used as a default name for components created of this type.
 */
std::string 
// [override]
mace::MoveXComponent::getTypeName() {
    return "Move X Component";
}


//
// PROTECTED MEMBER FUNCTIONS - EVENTS
//

/**
 * @fn MoveXComponent::initControls()
 * @brief Initialize controls for this Move X Component.
 * @author Joshua McLean
 * @date 2012/10/06 16:54
 *
 * Binds the left and right arrow keys to moving negatively and positively, respectively, along the
 * X axis.
 */
void
// [override]
mace::MoveXComponent::initControls() {
    auto pInputSystem = Component::findComponentOfType< Input >();

    Input::CommandFunc leftFunc = std::bind( &MoveXComponent::moveNegativeHeld, this );
    pInputSystem->addCommand( leftFunc, "MoveLeft", Input::HELD, OIS::KC_LEFT );

    Input::CommandFunc rightFunc = std::bind( &MoveXComponent::movePositiveHeld, this );
    pInputSystem->addCommand( rightFunc, "MoveRight", Input::HELD, OIS::KC_RIGHT );
} // void mace::MoveXComponent::initControls()
