#include "mace/Color.h"


//
// PUBLIC STATIC DATA - CONSTANTS
//

const mace::Color mace::Color::BLACK = mace::Color( 0.0f );
const mace::Color mace::Color::BLUE = mace::Color( 0.0f, 0.0f, 1.0f );
const mace::Color mace::Color::GREEN = mace::Color( 0.0f, 1.0f, 0.0f );
const mace::Color mace::Color::RED = mace::Color( 1.0f, 0.0f, 0.0f );
const mace::Color mace::Color::TRANSPARENT_BLACK = mace::Color( 0.0f, 0.0f );
const mace::Color mace::Color::TRANSPARENT_WHITE = mace::Color( 1.0f, 0.0f );
const mace::Color mace::Color::WHITE = mace::Color( 1.0f );


//
// PUBLIC CTOR
//

/**
 * @fn Color::Color( const float, const float )
 * @brief Default, brightness/alpha constructor.
 * @param a_brightness The brightness level (value for each of RGB, default 0.0f or black).
 * @param a_alpha The alpha level (default 1.0f).
 * @author Joshua McLean
 * @date 2012/10/08 15:33
 *
 * Create a color with the given brightness for red, green, and blue, and the given alpha value.
 */
mace::Color::Color( 
    const float a_brightness, // = 0.0f
    const float a_alpha // = 1.0f 
) : 
    r( a_brightness ), 
    g( a_brightness ), 
    b( a_brightness ), 
    a( a_alpha ) 
{} // mace::Color::Color( const float a_brightness, const float a_alpha ) 


/**
 * @fn Color::Color( const float, const float, const float, const float )
 * @brief Specific color constructor.
 * @param a_red The red value.
 * @param a_green The green value.
 * @param a_blue The blue value.
 * @param a_alpha The (optional) alpha value (default 1.0f).
 * @author Joshua McLean
 * @date 2012/10/08 15:34
 *
 * Create a color with the given red, green, blue, and alpha values.
 */
mace::Color::Color( 
    const float a_red, 
    const float a_green, 
    const float a_blue, 
    const float a_alpha // = 1.0f
) : 
    r( a_red ), 
    g( a_green ), 
    b( a_blue ), 
    a( a_alpha ) 
{} // mace::Color::Color( const float a_red, const float a_green, const float a_blue, 
    // const float a_alpha )


//
// PUBLIC WRAPPER COPY CTOR
//

/**
 * @fn Color::Color( const Ogre::ColourValue & )
 * @brief Create a color converted from an OGRE ColourValue.
 * @param a_rOgreColor The OGRE ColourValue from which this will be created.
 * @author Joshua McLean
 * @date 2012/10/08 15:35
 *
 * Construct a color using the red, green, blue, and alpha values from the given OGRE ColourValue.
 */
mace::Color::Color( const Ogre::ColourValue &a_rOgreColor ) :
    r( a_rOgreColor.r ),
    g( a_rOgreColor.g ),
    b( a_rOgreColor.b ),
    a( a_rOgreColor.a )
{} // mace::Color::Color( const Ogre::ColourValue &a_rOgreColor )


//
// PUBLIC MEMBER FUNCTIONS - WRAPPER CONVERTER
//

/**
 * @fn Color::toOgreColor()
 * @brief Convert this color to an OGRE ColourValue.
 * @return An OGRE ColourValue equivalent to this color.
 * @author Joshua McLean
 * @date 2012/10/08 15:36
 *
 * Construct an OGRE ColourValue with our red, green, blue, and alpha values, then return it.
 */
Ogre::ColourValue 
mace::Color::toOgreColor() {
    return Ogre::ColourValue( r, g, b, a );
} // Ogre::ColourValue mace::Color::toOgreColor()
