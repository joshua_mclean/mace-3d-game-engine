Joshua McLean
Senior Project 2012
MACE and MACE Test Game

Execute MACE_Program/MACE_TestGame to run the program.
Documentation and full source provided in MACE_Documentation.pdf.
Source files in MACE_Source.
NOTE: Include files are missing. I will upload these as soon as I find the backup.

CONTROLS
Move the gray pillar: arrow keys
Exit the program: Esc

KNOWN BUGS
Sometimes, after the falling objects collid with the pillar, the controls no
longer work.
